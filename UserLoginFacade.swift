//
//  UserLoginFacade.swift
//  TestingSwift
//
//  Created by Kemar White on 2/15/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import HTZNetworkingManager
import Result
import SwiftyJSON
import ObjectMapper

class UserLoginFacade: HTZNetworkingFacade {

    static let userLoginSharedInstance = UserLoginFacade()

    var isLoggedIn = false

    override init()
    {
        super.init()
        networkingManager.baseURL = "\(VehicleEndpoint.testingURL.rawValue)\(VehicleEndpoint.UserBaseURL.rawValue)"
    }

    func loginUser(user: User, loginResponse: Result<Bool, LoginStatusError> -> () )
    {
        let jsonData: [String: AnyObject] = ["username": user.username, "password": user.password]
        let endpoint = "/login"
        networkingManager.sendDataWithEndPoint(endpoint, dataParameters: jsonData, responseType: .String, httpMethod: HTTPRequestType.POST) { (responseData) -> () in
            switch responseData {
            case .Success(let status):
                guard let statusCode =  LoginStatusCode(rawValue: Int((status as! String))!) else {
                    loginResponse(.Failure(LoginStatusError.Unknown("An invalid status code was received. - \((status as! String))")))
                    return
                }
                // Enumerate through the status codes
                switch statusCode {
                case .Successful:
                    loginResponse(.Success(true))
                case .InvalidUsername:
                    loginResponse(.Failure(LoginStatusError.InvalidUsername))
                case .InvalidPassword:
                    loginResponse(.Failure(LoginStatusError.InvalidPassword))
                case .Unknown:
                    loginResponse(.Failure(LoginStatusError.Unknown("Unknown status code received. - \(statusCode.rawValue)")))
                }
            case .Failure(let NetworkingError.ResponseError(responseError)):
                loginResponse(.Failure(LoginStatusError.Unknown(responseError)))
            }
        }
    }
}
