//
//  ProjectExtensions.swift
//  TestingSwift
//
//  Created by Kemar White on 7/26/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationDistance
{
    /// 1 mile distance in meters.
    public var kCLLocationDistanceMile: CLLocationDistance {
        get
        {
            return 1609.34
        }
    }
}