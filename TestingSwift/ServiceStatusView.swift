//
//  ServiceStatusView.swift
//  TestingSwift
//
//  Created by Kemar White on 10/18/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit

class ServiceStatusView: UIView {

    @IBOutlet weak var vehicleStatusLabel: UILabel!

    @IBOutlet weak var image: UIImageView!
    
}
