//
//  YelpSearch.swift
//  TestingSwift
//
//  Created by Kemar White on 12/29/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import ObjectMapper

struct SearchedBusiness: Mappable {
    let urlTransform = URLTransform()
    let rawPrefix = "businesses.0."
    var name: String?
    var imageURL: NSURL?
    var phone: String?

    init?(_ map: Map) {}

    mutating func mapping(map: Map) {
        name <- map[rawPrefix+"name"]
        imageURL <- (map[rawPrefix+"image_url"], TransformOf<NSURL, String>(fromJSON: { NSURL(string: $0!) }, toJSON: {$0.map {$0.absoluteString } } ) )
        phone <- map[rawPrefix+"phone"]
    }
}