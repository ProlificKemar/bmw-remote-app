//
//  VehicleDetailViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 1/25/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import UIKit

class VehicleDetailViewController: UIViewController {

    @IBOutlet weak var modelYearNameLabel: UILabel!

    @IBOutlet weak var modelStyleLabel: UILabel!

    @IBOutlet weak var VINLabel: UILabel!

    @IBOutlet weak var fuelRangeLabel: UILabel!

    @IBOutlet weak var fuelConsumptionLabel: UILabel!

    @IBOutlet weak var odometerLabel: UILabel!

    @IBOutlet weak var vehicleImageView: UIImageView!

    @IBOutlet weak var lastUpdatedLabel: UILabel!

    @IBOutlet weak var odometerInnerStackView: UIStackView!

    private lazy var loadingScreenManager = LoadingScreenManager()

    private let dataController = VehicleDetailDataController()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "MV")!)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        setupUI()
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle
    {
        return UIStatusBarStyle.LightContent
    }

    @IBAction func refreshView(sender: UIButton) {
        loadingScreenManager.displayLoadingScreen("Refreshing Vehicle", parentView: view)
        dataController.retrieveVehicleFromDatabase(shouldReloadManually: true) { (responseData) in
            self.loadingScreenManager.hideLoadingScreen()
            switch responseData {
            case .Success(let vehicleData):

                // Check what kind of vehicle was received

                for vName in VehicleModel.allValues {
                    let vehicleTransformer = VehicleTransformer()
                    if vName.rawValue == vehicleData.name {

                        if vehicleData.dynamicType == RegularVehicle.self {
                            let tempVehicle = vehicleTransformer.validateRegularVehicle(vehicleData as! RegularVehicle)
                            if tempVehicle.error != nil {
                                print(tempVehicle.error!)
                                return
                            }
                            guard let regVehicle = tempVehicle.value else {
                                print(tempVehicle.error!)
                                return
                            }
                            self.fuelRangeLabel.text = String(regVehicle.fuelRange)
                            self.fuelConsumptionLabel.text = String(regVehicle.fuelConsumption)
                            self.vehicleImageView.image = self.dataController.retrieveVehicleImage(vName)
                            self.modelYearNameLabel.text = "\(String(regVehicle.manufacturedYear))' \(regVehicle.name)"
                            let numberFormatter = NSNumberFormatter()
                            numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                            self.odometerLabel.text = numberFormatter.stringFromNumber(regVehicle.odometer)
                            self.VINLabel.text = regVehicle.VIN
                            self.lastUpdatedLabel.text = regVehicle.lastUpdated
                        } else {
                            let tempVehicle = vehicleTransformer.validateElectricVehicle(vehicleData as! ElectricVehicle)
                            if tempVehicle.error != nil {
                                print(tempVehicle.error!)
                                return
                            }
                            guard let eVehicle = tempVehicle.value else {
                                print(tempVehicle.error!)
                                return
                            }
                            self.fuelRangeLabel.text = String(eVehicle.electricRange)
                            self.fuelConsumptionLabel.text = String(eVehicle.electricConsumption)
                            self.vehicleImageView.image = self.dataController.retrieveVehicleImage(vName)
                            self.modelYearNameLabel.text = "\(String(eVehicle.manufacturedYear))' \(eVehicle.name)"
                            let numberFormatter = NSNumberFormatter()
                            numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                            self.odometerLabel.text = numberFormatter.stringFromNumber(eVehicle.odometer)
                            self.VINLabel.text = eVehicle.VIN
                            self.lastUpdatedLabel.text = eVehicle.lastUpdated
                        }

                        // If the No Connection View is already on screen remove it

                        for foundView in self.view.subviews {
                            let noConnectionView = UIView.fromNib() as NoConnectionScreenView
                            if foundView.dynamicType == noConnectionView.dynamicType {
                                foundView.removeFromSuperview()
                            }
                        }
                    }
                }
            case .Failure(let VehicleNetworkingError.UnknownNetworkError(stringError)):
                if stringError == VehicleErrorDescription.ConnectionError.rawValue {
                    let noConnectionView = UIView.fromNib() as NoConnectionScreenView
                    noConnectionView.translatesAutoresizingMaskIntoConstraints = false
                    self.view.addSubview(noConnectionView)
                    noConnectionView.setupWithError(VehicleErrorDescription.ConnectionError)

                    NibDisplayManager.displayViewForViewController(noConnectionView, viewControllerView: self.view)
                    self.view.bringSubviewToFront(noConnectionView)
                }
            case .Failure(VehicleNetworkingError.CannotParseVehicle):
                print("Invalid vehicle returned.")
            case .Failure(VehicleNetworkingError.NoVehiclesFound):
                print("No vehicles were found.")
            default:
                print("An unknown error occurred")
            }
        }
    }

    private func setupUI()
    {
        // Need to programatically set allignment since causes a possible crash in IB
        odometerInnerStackView.alignment = .Trailing
        dataController.retrieveVehicleFromDatabase(shouldReloadManually: false) { (responseData) in
            switch responseData {
            case .Success(let vehicleData):

                // Check what kind of vehicle was received

                for vName in VehicleModel.allValues {
                    let vehicleTransformer = VehicleTransformer()
                    if vName.rawValue == vehicleData.name {

                        if vehicleData.dynamicType == RegularVehicle.self {
                            let tempVehicle = vehicleTransformer.validateRegularVehicle(vehicleData as! RegularVehicle)
                            if tempVehicle.error != nil {
                                print(tempVehicle.error!)
                                return
                            }
                            guard let regVehicle = tempVehicle.value else {
                                print(tempVehicle.error!)
                                return
                            }
                            self.fuelRangeLabel.text = String(regVehicle.fuelRange)
                            self.fuelConsumptionLabel.text = String(regVehicle.fuelConsumption)
                            self.vehicleImageView.image = self.dataController.retrieveVehicleImage(vName)
                            self.modelYearNameLabel.text = "\(String(regVehicle.manufacturedYear))' \(regVehicle.name)"
                            let numberFormatter = NSNumberFormatter()
                            numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                            self.odometerLabel.text = numberFormatter.stringFromNumber(regVehicle.odometer)
                            self.VINLabel.text = regVehicle.VIN
                            self.lastUpdatedLabel.text = regVehicle.lastUpdated
                        } else {
                            let tempVehicle = vehicleTransformer.validateElectricVehicle(vehicleData as! ElectricVehicle)
                            if tempVehicle.error != nil {
                                print(tempVehicle.error!)
                                return
                            }
                            guard let eVehicle = tempVehicle.value else {
                                print(tempVehicle.error!)
                                return
                            }
                            self.fuelRangeLabel.text = String(eVehicle.electricRange)
                            self.fuelConsumptionLabel.text = String(eVehicle.electricConsumption)
                            self.vehicleImageView.image = self.dataController.retrieveVehicleImage(vName)
                            self.modelYearNameLabel.text = "\(String(eVehicle.manufacturedYear))' \(eVehicle.name)"
                            let numberFormatter = NSNumberFormatter()
                            numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                            self.odometerLabel.text = numberFormatter.stringFromNumber(eVehicle.odometer)
                            self.VINLabel.text = eVehicle.VIN
                            self.lastUpdatedLabel.text = eVehicle.lastUpdated
                        }

                        // If the No Connection View is already on screen remove it

                        for foundView in self.view.subviews {
                            let noConnectionView = UIView.fromNib() as NoConnectionScreenView
                            if foundView.dynamicType == noConnectionView.dynamicType {
                                foundView.removeFromSuperview()
                            }
                        }
                    }
                }
            case .Failure(let VehicleNetworkingError.UnknownNetworkError(stringError)):
                if stringError == VehicleErrorDescription.ConnectionError.rawValue {
                    let noConnectionView = UIView.fromNib() as NoConnectionScreenView
                    noConnectionView.translatesAutoresizingMaskIntoConstraints = false
                    self.view.addSubview(noConnectionView)
                    noConnectionView.setupWithError(VehicleErrorDescription.ConnectionError)

                    NibDisplayManager.displayViewForViewController(noConnectionView, viewControllerView: self.view)
                    self.view.bringSubviewToFront(noConnectionView)
                }
            case .Failure(VehicleNetworkingError.CannotParseVehicle):
                print("Invalid vehicle returned.")
            case .Failure(VehicleNetworkingError.NoVehiclesFound):
                print("No vehicles were found.")
            default:
                print("An unknown error occurred")
            }
        }
    }
}
