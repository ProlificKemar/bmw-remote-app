//
//  YelpAuthenticator.swift
//  TestingSwift
//
//  Created by Kemar White on 12/22/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation

protocol OAuth1enticatable {
    var consumerKey: String { get }
    var consumerSecret: String { get }
    var token: String { get }
    var tokenSecret: String { get }
}