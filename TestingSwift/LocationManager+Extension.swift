//
//  LocationManager+Extension.swift
//  TestingSwift
//
//  Created by Kemar White on 12/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import Contacts.CNPostalAddress
import CoreLocation

extension LocationManager {

    func transformPostalAddressForCoordinate(location: CLLocation, createdAddress: (CNPostalAddress?) -> () )
    {
        let postalAddress = CNMutablePostalAddress()
        let geocoder = CLGeocoder()

        geocoder.reverseGeocodeLocation(location) { (foundPlacemark, error) -> Void in
            if error != nil {
                print("An error occurred reverse locating the given location - \(error!.localizedDescription)")
                return
            }else if foundPlacemark?.isEmpty == false && foundPlacemark?.first != nil {
                guard let dict = foundPlacemark?.first?.addressDictionary else { return }
                postalAddress.city = dict["City"] as! String
                postalAddress.street = dict["Street"] as! String
                postalAddress.state = dict["State"] as! String
                postalAddress.postalCode = dict["PostCodeExtension"] as! String
                postalAddress.country = dict["Country"] as! String
                postalAddress.ISOCountryCode = dict["CountryCode"] as! String

                createdAddress(postalAddress as CNPostalAddress?)
            }
        }
    }
}