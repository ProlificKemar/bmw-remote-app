//
//  LocationManager.swift
//  TestingSwift
//
//  Created by Kemar White on 6/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class LocationManager: CLLocationManager, CLLocationManagerDelegate {

    static let sharedInstance = LocationManager()

    var currentLocation: CLLocation? {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            if let locationExists = self.location {
                return locationExists
            }
        }
        return nil
    }

    #if IOS
    @available(iOS 8.0, *)
    /**
    Center in on a specified map location via its **CLPlacemark**.

    - parameter mapView:   The **MKMapView** for the given location.
    - parameter placemark: The specified **CLPlacemark**.
    - parameter animated:  Specifies whether the centering on the specified location should be animated.
    */
    class func centerOnLocation(mapView: MKMapView, placemark: CLPlacemark, animated: Bool)
    {
    if let location = placemark.location {
    let coordinateRegion: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500)
    mapView.setRegion(coordinateRegion, animated: animated)
        }
    }

    #endif

    // MARK: Initializers

    override init() {
        super.init()
        desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        distanceFilter = 100
        delegate = self
    }
    
    // MARK: CLLocationManagerDelegate Methods

    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("An error occurred with the location service \(error.localizedDescription)")
    }

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
            if CLLocationManager.authorizationStatus() == .NotDetermined {
                requestWhenInUseAuthorization()
            }
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
      print("New location is \(locations.last)")
    }
}
