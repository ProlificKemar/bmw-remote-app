//
//  FetchedDataController.swift
//  TestingSwift
//
//  Created by Kemar White on 10/22/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Result

class FetchedDataController {

    static let globalInstance = FetchedDataController()

    lazy var vehicle: BMWVehicle? = {
        [unowned self] in
            if !AppLaunch.isFirstLaunch {
                guard let createdVehicle = self.createBMWVehicleModel() else { return nil }
                return createdVehicle
            } else {
                guard let loadedVehicle = self.loadBMWModel() else { return nil }
                return loadedVehicle
            }
    }()

   final var vehicleLocationCount: Int {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let fetchRequest = NSFetchRequest(entityName: CoreDataModel.VehicleLocation.rawValue)
    let error: NSErrorPointer = nil

    return appDelegate.managedObjectContext.countForFetchRequest(fetchRequest, error: error)
    }

    private func createBMWVehicleModel() -> BMWVehicle?
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        // We're using a mocked model here to pupolate the DB for example purposes
        let vehicle = NSEntityDescription.insertNewObjectForEntityForName(CoreDataModel.Vehicle.rawValue, inManagedObjectContext:appDelegate.managedObjectContext) as! Vehicle
        let testVehicleModel = RegularVehicle(name: "X5 50i", odometer: 5402, nextServiceMileage: 2500, vehicleVIN: "A9DJ283JF8109DJ8H3K", vehicleManufacturedYear: 13, fuelRange: 250, fuelConsumption: 24, lastUpdated: NSDate.shortenedDateAndTimeFormat(NSDate()))
        vehicle.name = testVehicleModel.name
        vehicle.vin = testVehicleModel.VIN
        vehicle.fuelRange = testVehicleModel.fuelRange
        vehicle.fuelLevel = testVehicleModel.fuelConsumption
        vehicle.nextServiceMileage = testVehicleModel.nextServiceMileage
        vehicle.odometer = testVehicleModel.odometer
        vehicle.lastUpdated = testVehicleModel.lastUpdated
        do {
            try appDelegate.managedObjectContext.save()
            return testVehicleModel
        } catch {
            print("An error occurred trying to save the vehicle object model")
        }
        return nil
    }

    private func loadBMWModel() -> BMWVehicle?
    {
        // Vehicle database housekeeping (** Needs to be tested **)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        let fetchRequest = NSFetchRequest(entityName: CoreDataModel.Vehicle.rawValue)
        do {
            let vehicles = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as! [Vehicle]
            // Remove all entities prior to last one (housekeeping)
            if vehicles.count > 1 {
                for (entityIndex, entity) in vehicles.enumerate() {
                    if ( (entityIndex + 1) != vehicles.count) {
                        appDelegate.managedObjectContext.deleteObject(entity)
                    }
                }
            }
            // Save the changes back to the context
            do {
                try appDelegate.managedObjectContext.save()
                if let foundVehicle = vehicles.last {
                    // Do checks to see what kind of vehicle and return it
                    if foundVehicle.electricCharge != 0 {
                        return ElectricVehicle(name: foundVehicle.name!, odometer: foundVehicle.odometer!.integerValue, nextServiceMileage: foundVehicle.nextServiceMileage!.integerValue, vehicleVIN: foundVehicle.vin!, vehicleManufacturedYear: foundVehicle.manufacturedYear!.integerValue, electricRange: foundVehicle.electricRange!.integerValue, totalRange: foundVehicle.electricRange!.integerValue, electricConsumption: foundVehicle.electricConsumption!.integerValue, electricCharge: foundVehicle.electricCharge!.integerValue, lastUpdated: foundVehicle.lastUpdated!)
                    } else {
                        // Is a non electric vehicle
                        return RegularVehicle(name: foundVehicle.name!, odometer: foundVehicle.odometer!.integerValue, nextServiceMileage: foundVehicle.nextServiceMileage!.integerValue, vehicleVIN: foundVehicle.vin!, vehicleManufacturedYear: foundVehicle.manufacturedYear!.integerValue, fuelRange: foundVehicle.fuelRange!.integerValue, fuelConsumption: foundVehicle.fuelLevel!.integerValue, lastUpdated: foundVehicle.lastUpdated!)
                    }
                }
            } catch {
                print("An error occurred trying to save the vehicle location object model")
            }
        } catch {
            // Catch error
        }
        return nil
    }

    func createVehicleLocationModel(currentVehicleLocation: VehicleLocation, saveStatus: (Bool) -> () )
    {
       let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        appDelegate.managedObjectContext.performBlock { () -> Void in
            let vLocation = NSEntityDescription.insertNewObjectForEntityForName(CoreDataModel.VehicleLocation.rawValue, inManagedObjectContext: appDelegate.managedObjectContext) as! VehicleLocationEntity
            vLocation.latitude! = NSNumber(double: currentVehicleLocation.location.latitude)
            vLocation.longitude! = NSNumber(double: currentVehicleLocation.location.longitude)
            vLocation.purgeable = currentVehicleLocation.purgeable
            vLocation.name = currentVehicleLocation.name
            vLocation.address = currentVehicleLocation.addressLiteral
            if let currentVehicleLocationAddressDictExists = currentVehicleLocation.addressDictionary {
                vLocation.addressDictionary = currentVehicleLocationAddressDictExists
            }
            do {
                try appDelegate.managedObjectContext.save()
                AppLaunch.setUserDefaultsBoolForKey(true, userDefaultsKey: AppLaunch.Keys.vehicleLocationKey)
                saveStatus(true)
            } catch {
                saveStatus(false)
                print("An error occurred trying to save the vehicle location object model")
            }
        }
    }

    /**
     Loads and returns the last known vehicle location.

     - returns: A  **VehicleLocation** object.
     */
    func loadVehicleLocationModel() -> VehicleLocation?
    {

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        let fetchRequest = NSFetchRequest(entityName: CoreDataModel.VehicleLocation.rawValue)

        do {
            let locations = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as! [VehicleLocationEntity]

        if FetchedDataController.globalInstance.vehicleLocationCount > 1 {
            // House keep entities to just a single vehicle location
            for (entityIndex, entity) in locations.enumerate() {
                if ( (entityIndex + 1) != locations.count) {
                    appDelegate.managedObjectContext.deleteObject(entity)
                }
            }
            // Save the changes back to the context
            do {
                try appDelegate.managedObjectContext.save()
            } catch {
                print("An error occurred trying to save the vehicle location object model")
            }
        }
            if let lastVehicleLocation = locations.last {
                let retrievedVehicleLocation = VehicleLocation()

                // TODO: Need to make sure that latitude and longitude are not ever nil
                if lastVehicleLocation.latitude != nil || lastVehicleLocation.longitude != nil {
                     retrievedVehicleLocation.location = CLLocationCoordinate2D(latitude: lastVehicleLocation.latitude!.doubleValue, longitude: lastVehicleLocation.longitude!.doubleValue)
                }
                retrievedVehicleLocation.purgeable = lastVehicleLocation.purgeable.boolValue
                retrievedVehicleLocation.name = lastVehicleLocation.name
                retrievedVehicleLocation.addressLiteral = lastVehicleLocation.address
                if let retrievedVehicleLocationAddressDictExists = lastVehicleLocation.addressDictionary {
                    retrievedVehicleLocation.addressDictionary = retrievedVehicleLocationAddressDictExists
                }
                return retrievedVehicleLocation
            }
        } catch {
            print("An error occurred trying to retrieve the vehicle location model from the CD database")
        }
        return nil
    }

    func loadVehicleLocationModelHouseKeepBackground() -> VehicleLocation?
    {

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        let fetchRequest = NSFetchRequest(entityName: CoreDataModel.VehicleLocation.rawValue)

        do {
            let mocContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
            mocContext.persistentStoreCoordinator = appDelegate.persistentStoreCoordinator
            let locations = try mocContext.executeFetchRequest(fetchRequest) as! [VehicleLocationEntity]
            if FetchedDataController.globalInstance.vehicleLocationCount > 1 {
                dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), {

                    // House keep entities to just a single vehicle location
                    mocContext.performBlock(){
                        for (entityIndex, entity) in locations.enumerate() {
                            if ( (entityIndex + 1) != locations.count) {
                                mocContext.deleteObject(entity)
                            }
                        }
                        // Save the changes back to the context
                        do {
                            try mocContext.save()
                        } catch {
                            print("An error occurred trying to save the vehicle location object model")
                        }
                    }
                })
            }

            if let lastVehicleLocation = locations.last {
                let retrievedVehicleLocation = VehicleLocation()

                // TODO: Need to make sure that latitude and longitude are not ever nil

                retrievedVehicleLocation.location = CLLocationCoordinate2D(latitude: lastVehicleLocation.latitude!.doubleValue, longitude: lastVehicleLocation.latitude!.doubleValue)
                retrievedVehicleLocation.purgeable = lastVehicleLocation.purgeable.boolValue
                retrievedVehicleLocation.name = lastVehicleLocation.name
                retrievedVehicleLocation.addressLiteral = lastVehicleLocation.address
                if let retrievedVehicleLocationAddressDictExists = lastVehicleLocation.addressDictionary {
                    retrievedVehicleLocation.addressDictionary = retrievedVehicleLocationAddressDictExists
                }
                return retrievedVehicleLocation
            }
        } catch {
            print("An error occurred trying to retrieve the vehicle location model from the CD database")
        }
        return nil
    }

    /**
     Deletes all entries within an **NSPersistentStore**.

     - parameter persistentStore: The persistent store that should be purged.
     */
    func purgePersistentStore(persistentStoreDatabase: CoreDataSQLDatabase, deleteStatus: (Bool) -> () )
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        appDelegate.managedObjectContext.performBlock { () -> Void in
            let storeName = appDelegate.applicationDocumentsDirectory.URLByAppendingPathComponent(persistentStoreDatabase.rawValue)

            // Set our fetch request entity based on the SQL database
            var entityName: String {
                var tempRequest = ""
                switch persistentStoreDatabase {
                case .VehicleModel:
                    tempRequest = CoreDataModel.Vehicle.rawValue
                case .VehicleLocationModel:
                    tempRequest = CoreDataModel.VehicleLocation.rawValue
                }
                return tempRequest
            }
            let request = NSFetchRequest(entityName: entityName)

            do {
                let entities = try appDelegate.managedObjectContext.executeFetchRequest(request) as! [NSManagedObject]

                for entity in entities
                {
                    appDelegate.managedObjectContext.deleteObject(entity)
                }
            } catch {
                print("An error occurred trying to delete objects from the \(persistentStoreDatabase.rawValue) the CD database")
            }
        }
        // Save back changes 
        do {
            try appDelegate.managedObjectContext.save()
            AppLaunch.setUserDefaultsBoolForKey(false, userDefaultsKey: AppLaunch.Keys.vehicleLocationKey)
            deleteStatus(FetchedDataController.globalInstance.vehicleLocationCount > 0)
        } catch {
            print("An error occurred trying to save the object model")
        }
    }
}

struct StaticDispatch {
    /// Static dispatch_once_t token.
    static var token: dispatch_once_t = 0
}
