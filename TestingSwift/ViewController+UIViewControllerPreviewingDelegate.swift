//
//  ViewController+UIViewControllerPreviewingDelegate.swift
//  TestingSwift
//
//  Created by Kemar White on 9/14/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit

extension ViewController: UIViewControllerPreviewingDelegate {

    // Create view controller that will be shown for the peek touch
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        // Get index path of pressed cell 
        guard let indexPath = dealershipsTableView.indexPathForRowAtPoint(location),
            cell = dealershipsTableView.cellForRowAtIndexPath(indexPath) else { return nil }

        // Create the detail VC and set the properties
        guard let detailVC = storyboard?.instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.detailViewController.rawValue) as? DealershipDetailViewController else { return nil }
        // Needs to be tested
        let choseCenter = foundServiceCenters?[indexPath.row]
        if let previewCenter = choseCenter {
        detailVC.setupView(previewCenter)
        }
        detailVC.preferredContentSize = CGSize(width: 0.0, height: 0.0)

        // Set source rect to cell frame to blur everything else
        previewingContext.sourceRect = cell.frame

        return detailVC
    }

    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        showViewController(viewControllerToCommit, sender: self)
    }
}
