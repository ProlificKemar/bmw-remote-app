//
//  YelpImage.swift
//  TestingSwift
//
//  Created by Kemar White on 12/28/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import UIKit

struct YelpImage {
    let name: String?
    let image: UIImage?
}