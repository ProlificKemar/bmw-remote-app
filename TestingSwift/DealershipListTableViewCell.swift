//
//  DealershipListTableViewCell.swift
//  TestingSwift
//
//  Created by Kemar White on 7/26/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit

class DealershipListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    func setupCellWithServiceCenter(center: ServiceCenter?)
    {
        titleLabel.text = center?.name
        subtitleLabel.text = ""
        if let authCenter = center as? AuthorizedServiceCenter {
            subtitleLabel.text = authCenter.number
        } else if let nonAuthCenter = center as? UnAuthorizedServiceCenter {
            subtitleLabel.text = nonAuthCenter.number
        }
        self.backgroundColor = UIColor.blackColor()
        self.titleLabel.font = UIFont(name: "Avenir Next", size: 15)
        self.titleLabel.textColor = UIColor.whiteColor()
        self.subtitleLabel.font = self.titleLabel.font
        self.subtitleLabel.textColor = Theme(rawValue: Theme.Electric.rawValue)?.color
    }
}