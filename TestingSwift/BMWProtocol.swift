//
//  BMWProtocol.swift
//  TestingSwift
//
//  Created by Kemar White on 12/2/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

protocol BMW {
    var name: String { get }
    var odometer: Int { get }
    var nextServiceMileage: Int { get }
    var VIN: String { get}
    var manufacturedYear: Int { get }
    var lastUpdated: String { get }
}

extension BMW where Self : Equatable {}

func ==<T: BMW>(lhs: T, rhs: T) -> Bool {
    if lhs.VIN == rhs.VIN {
        return true
    } else {
        return false
    }
}

protocol RegularCar: BMW {
    var fuelRange: Int { get }
    var fuelConsumption: Int { get }
}

protocol ElectricCar: BMW {
    var electricRange: Int { get }
    var totalRange: Int { get }
    var electricConsumption: Int { get }
    var electricCharge: Int { get }
}
