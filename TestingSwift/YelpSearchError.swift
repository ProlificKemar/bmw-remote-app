//
//  YelpSearchError.swift
//  TestingSwift
//
//  Created by Kemar White on 1/4/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

enum YelpSearchError: ErrorType {
    case searchFailed(String)
    case validationFailed
    case invalidImageData
    case ServerSideError
}