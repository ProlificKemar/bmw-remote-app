//
//  NoConnectionScreenView.swift
//  TestingSwift
//
//  Created by Kemar White on 1/31/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import UIKit

class NoConnectionScreenView: UIView {

    @IBOutlet weak var backgroundView: UIVisualEffectView!

    @IBOutlet weak var connectionTextLabel: UILabel!
    
    override func awakeFromNib()
    {
        backgroundView.backgroundColor = UIColor(patternImage: UIImage(named: "MV")!)
    }

    func setupWithError(errorDescription: VehicleErrorDescription)
    {
        connectionTextLabel.text = errorDescription.rawValue
    }
}