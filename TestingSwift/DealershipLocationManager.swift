//
//  DealershipLocationManager.swift
//  TestingSwift
//
//  Created by Kemar White on 7/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Result

class DealershipLocationManager: LocationManager {

    static let globalInstance = DealershipLocationManager()
    
    private func pinAnnotationFromSearchRequest(searchResponse: MKLocalSearchResponse?) -> [MKPointAnnotation]?
    {
        var annotations = [MKPointAnnotation]()
        let mappedItems = searchResponse?.mapItems
        for mapItem in mappedItems! {
            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.name
            annotation.subtitle = mapItem.placemark.thoroughfare
            annotations.append(annotation)
        }
        return annotations
    }

    private func pinAnnotationsForMapItems(mapItems: [MKMapItem]) -> [MKPointAnnotation]
    {
        var annotations = [MKPointAnnotation]()
        for mapItem in mapItems {
            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.name
            annotation.subtitle = mapItem.placemark.thoroughfare
            annotations.append(annotation)
        }
        return annotations
    }

    //MARK: Public Methods

    /**
    Search for dealership locations and returns a closure with the found Pin annotations for any dealerships nearby.

    - parameter dealershipName:    The location name that you are looking for.
    - parameter completionClosure: The completion closure that will return the pin annotations when found.
    */
    func searchForDealership(dealershipName: String, completionClosure: (foundLocations: [MKPointAnnotation]?, foundCenters: [ServiceCenter]? ) -> () )
    {
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = dealershipName

        let search = MKLocalSearch(request: searchRequest)
        search.startWithCompletionHandler { (searchResponse: MKLocalSearchResponse?, error: NSError?) -> Void in
            if searchResponse != nil {
                // Deliver search response

                var locatedCenters: [ServiceCenter]? = []

                for response in searchResponse!.mapItems where response.name != nil {
                    if response.phoneNumber != nil && response.url != nil {
                        let aCenter = AuthorizedCenter(name: response.name!, location: response.placemark.coordinate, number: response.phoneNumber!, url:response.url!, locationDetails: response.placemark.addressDictionary)
                        locatedCenters?.append(aCenter)
                    } else {
                        let aCenter = IndependentCenter(location: response.placemark.coordinate, name: response.name!, number: response.phoneNumber, locationDetails: response.placemark.addressDictionary)
                        locatedCenters?.append(aCenter)
                    }
                }
                completionClosure(foundLocations: self.pinAnnotationFromSearchRequest(searchResponse), foundCenters: locatedCenters)
            }
        }
    }

    func searchForServiceCenters(dealershipName: String, completionClosure: (searchResuts: Result<SearchedServiceCenters, SearchedServiceCenterError>) -> () )
    {
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = dealershipName
        // Need to manually set the region here
        searchRequest.region = MKCoordinateRegionMake((BMWFacade.vehicleLocationManager.currentLocation?.coordinate)!, MKCoordinateSpanMake(0.1, 0.1))
        let search = MKLocalSearch(request: searchRequest)
        search.startWithCompletionHandler { (searchResponse: MKLocalSearchResponse?, error: NSError?) -> Void in

            if  error != nil {
                completionClosure(searchResuts: .Failure(SearchedServiceCenterError.SearchFailed(error!.localizedDescription)))
                return
            } else {
                // Deliver search response

                var locatedCenters: [ServiceCenter]? = []

                for response in searchResponse!.mapItems where response.name != nil {
                    if response.phoneNumber != nil && response.url != nil {
                        let aCenter = AuthorizedCenter(name: response.name!, location: response.placemark.coordinate, number: response.phoneNumber!, url:response.url!, locationDetails: response.placemark.addressDictionary)
                        locatedCenters?.append(aCenter)
                    } else {
                        let aCenter = IndependentCenter(location: response.placemark.coordinate, name: response.name!, number: response.phoneNumber, locationDetails: response.placemark.addressDictionary)
                        locatedCenters?.append(aCenter)
                    }
                }
                guard let mappedPins = searchResponse?.mapItems where searchResponse?.mapItems.isEmpty == false else {
                    completionClosure(searchResuts: .Failure(SearchedServiceCenterError.NoCentersFound))
                    return
                }
                let foundCenters = SearchedServiceCenters(centers: locatedCenters!, pins: self.pinAnnotationsForMapItems(mappedPins))
                completionClosure(searchResuts: .Success(foundCenters))
            }
        }
    }

    // MARK: CLLocationManagerDelegate Methods (must override below methods for -requestLocation()

    override func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("An error occurred with the location service \(error.localizedDescription)")
    }

    override func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            requestWhenInUseAuthorization()
        }
    }

    override func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {

    }
}
