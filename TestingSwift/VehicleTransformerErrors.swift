//
//  VehicleTransformerError.swift
//  TestingSwift
//
//  Created by Kemar White on 2/8/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

enum VehicleTransformerError: ErrorType {
    case TransformFailed(String)
}
