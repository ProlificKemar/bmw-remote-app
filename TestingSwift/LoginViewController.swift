//
//  LoginViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 2/14/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    //MARK: IBOutlets

    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var logoNameLabel: UILabel!
    
    @IBOutlet weak var usernameTextField: UITextField!

    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var loginButton: UIButton!

    @IBOutlet weak var forgottonPasswordButton: UIButton!

    @IBOutlet weak var createAccountButton: UIButton!

    //MARK: Vars

    private let nc = NSNotificationCenter.defaultCenter()

    private let mainQueue = NSOperationQueue.mainQueue()

    private var splashScreenImageView: UIImageView!

    private lazy var lSM = LoadingScreenManager()

    private weak var observer: NSObjectProtocol?

    private weak var delegate: ViewControllerDismissal?

    //MARK: View Loading Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        observer = nc.addObserverForName(UITextFieldTextDidChangeNotification, object: nil, queue: mainQueue) {[weak self] _ in
            self?.loginButton.enabled = self?.passwordTextField.text?.utf16.count == 4 && self?.usernameTextField.text?.utf16.count > 4
        }
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        modalTransitionStyle = .CrossDissolve
        animateUI()
    }

    //MARK: Private Methods

    private func animateUI()
    {
        splashScreenImageView = UIImageView(image: UIImage(named: "corona_rings"))
        splashScreenImageView.contentMode = .ScaleAspectFill
        view.addSubview(splashScreenImageView)
        NibDisplayManager.displayViewForViewController(splashScreenImageView, viewControllerView: view)
        NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(LoginViewController.fadeInView), userInfo: nil, repeats: false)
    }

    /*NOTE: First time used but I used @objc here to expose this private
     method to Objective-C to allow it to be used as a selector while retaining its private attribute.
     */
    @objc private func fadeInView()
    {
       UIView.animateWithDuration(0.825, delay: 0, usingSpringWithDamping: 0.625, initialSpringVelocity: 0.3, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
        self.splashScreenImageView.transform = CGAffineTransformMakeScale(0.01, 0.01)
        self.splashScreenImageView.alpha = 0
        }, completion: {(_) in
            self.splashScreenImageView = nil
        })
    }

    //MARK: Public Methods

    func setupDelegate(delegate: ViewControllerDismissal)
    {
        self.delegate = delegate
    }

    //MARK: IBActions

    @IBAction func loginUser(sender: UIButton)
    {
        testLogin()
    }

    private func testLogin()
    {
        let loginFacade = UserLoginFacade()
        /*
        Testable login: john.johnson@gmail.com pass: 1234
        */
        lSM.displayLoadingScreen("Logging In", parentView: view)
        loginFacade.loginUser(User(username: usernameTextField.text!, password: Int(passwordTextField.text!)!)) { (loginResponse) -> () in
            self.lSM.hideLoadingScreen()
            switch loginResponse {
            case .Success(let status):
                UserLoginFacade.userLoginSharedInstance.isLoggedIn = status
                self.delegate?.dismissViewController(self)
            case .Failure(LoginStatusError.InvalidUsername):
                AlertControllerManager.displayAlert(LoginAlertControllerMessage.InvalidUsername, onScreenViewController: self)
            case .Failure(LoginStatusError.InvalidPassword):
                AlertControllerManager.displayAlert(LoginAlertControllerMessage.InvalidPassword, onScreenViewController: self)
            case .Failure(let LoginStatusError.Unknown(errorString)):
                print(errorString)
                AlertControllerManager.displayAlert(LoginAlertControllerMessage.UnknownError, onScreenViewController: self)
            }
        }
    }

    //MARK: UITextFieldDelegate Methods

    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        return true
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            testLogin()
        }
        return true
    }
}

protocol ViewControllerDismissal: NSObjectProtocol {
    func dismissViewController(viewController: UIViewController)
}
