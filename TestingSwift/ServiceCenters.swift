//
//  ServiceCenters.swift
//  TestingSwift
//
//  Created by Kemar White on 11/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import CoreLocation

protocol ServiceCenter {
    var name: String { get }
    var location: CLLocationCoordinate2D { get }
    var locationDetails: [NSObject : AnyObject]? { get }
}

extension ServiceCenter where Self : Equatable {}

func ==<T: ServiceCenter>(lhs: T, rhs: T) -> Bool {
    if lhs.name == rhs.name && (lhs.location.latitude == rhs.location.latitude && lhs.location.longitude == lhs.location.longitude) {
        return false
    } else {
        return true
    }
}

protocol AuthorizedServiceCenter: ServiceCenter {
    var number: String { get }
    var url: NSURL { get }
}

protocol UnAuthorizedServiceCenter: ServiceCenter {
    var number: String? { get }
}
