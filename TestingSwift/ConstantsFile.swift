//
//  ConstantsFile.swift
//  TestingSwift
//
//  Created by Kemar White on 7/31/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

/*
Constants file to be used for the TestingSwift target and its equivalent WatchKit Extension target.

*/

import UIKit

// MARK: Structs

struct AppLaunch {
    enum Keys: String {
        case appFirstLaunchKey = "FirstLaunch"
        case vehicleLocationKey = "vehicleLocation"
    }
    static var isFirstLaunch: Bool {
        get {
       return NSUserDefaults.standardUserDefaults().boolForKey(AppLaunch.Keys.appFirstLaunchKey.rawValue)
        }
    }
    static var isVehicleLocationStored: Bool {
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey(AppLaunch.Keys.vehicleLocationKey.rawValue)
        }
    }

    /**
     Sets the bool value for a given *NSUserDefaults.standardUserDefaults()* key.

     - parameter boolValue:        The bool value to apply to the given key.
     - parameter userDefaultsKey: The *NSUserDefaults* key to apply the bool value upon.
     */
    static func setUserDefaultsBoolForKey(boolValue: Bool, userDefaultsKey: Keys)
    {
        NSUserDefaults.standardUserDefaults().setBool(boolValue, forKey: userDefaultsKey.rawValue)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}

// MARK: Enums

enum CoreDataModel: String {
    case Vehicle = "Vehicle"
    case VehicleLocation = "VehicleLocationEntity"
}

enum CoreDataSQLDatabase: String {
    case VehicleModel = "VehicleModel.sqlite"
    case VehicleLocationModel = "VehicleLocation.sqlite"
}

enum MessageKey: String {
    case Command = "command"
    case StateUpdate = "stateUpdate"
    case Dealerships = "dealerships"
    case Acknowledge = "ack"
    case RequestServiceCenters = "requestForServiceCenters"
    case RequestVehicleData = "requestVehicleData"
}

enum MessageCommand: String {
    case SendLocationStatus     = "sendLocationUpdateStatus"
    case StartUpdatingLocation  = "startUpdatingLocation"
    case StopUpdatingLocation   = "stopUpdatingLocation"
    case GetServiceCenters      = "getServiceCenters"
}

enum StoryboardName: String {
    case Main
    case UserAuthentication
}

enum MainStoryboardIdentifiers: String {
    case name = "Main"
    case detailViewController = "DetailViewController"
    case vehicleDetailStoryboardName = "DetailedVehicleInformation"
    case appointmentViewController = "AppointmentViewController"
    case vehiclePagingViewController = "VehicleDetailAndLocationViewController"
case fullScreenViewController = "FullScreenViewController"
    case contentScreenViewController = "ContentScreenViewController"
}

enum UserAuthenticationStoryBoardIdentifiers: String {
    case LoginVIewController
}

enum ViewControllersIdentifiers: String {
    case vehicleDetail = "VehicleDetail"
}

enum InterfaceControllerIdentifiers: String {
    case detailInterfaceController = "detailInterfaceController"
}

enum VehicleDataModel: String {
    case name
    case VIN
    case MY
    case lastModified
    case manufacturedYear
    case fuelLevel
    case fuelRange
    case nextServiceMileage
    case odometerReading
    case electricRange
    case totalRange
    case eletricConsumption
    case electricCharge
}

enum VehicleModel: String {
    case BMW_335 = "BMW 335i"
    case BMW_M2 = "BMW M2"
    case BMW_M3 = "BMW M3"
    case BMW_M4 = "BMW M4"
    case BMW_M5 = "BMW M5"

    static let allValues = [BMW_335, BMW_M2, BMW_M3, BMW_M4, BMW_M5]
}

enum Icon: String {
    case SUV = "SUV"
    case Walking = "Walking"
    case Driving = "Driving"
    case Expired = "Expired"
    case HighPriority = "HighPriority"
    case Phone = "Phone"
}

enum VehicleAnnotationViewIdentifier: String {
    case vehicleLocationView = "vehicleLocationViewReuseIdentifier"
    case serviceCenterView = "serviceCenterLocationViewReuseIdentifier"
    case independentCenterView = "individualCenterViewIdentifier"
}

enum VehicleErrorDescription: String {
    case ConnectionError = "Could not connect to the server."
    case NoVehicles = "No vehicles were found for your account"
    case InvalidResponse = "An internal server error occurred. Please try again later."
}

enum LoginAlertControllerMessage: String {
    case InvalidUsername = "The username provided does not exist. Please try again.."
    case InvalidPassword = "The password you provided is not associated with this account."
    case UnknownError = "An unknown error occurred. Please try again later."
}

enum Theme: Int {
    case Regular, Electric

    var color: UIColor {
        switch self {
        case .Regular:
        return UIColor(red: 207.0/255.0, green: 51.0/255.0, blue: 19.0/255.0, alpha: 1.0)

        case .Electric:
        return UIColor(red: 81.0/255.0, green: 146.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        }
    }
}

// MARK: Yelp

enum YelpKeys: String {
    case ConsumerKey = "ClGCW8Y0wHb-yEwVLKgIYg"
    case ConsumerSecret = "m-8RaJAqrAz3e5hCE6S7aOuzV40"
    case Token = "PB2uaRHSEquqO9b79gVgxXT1erAFrcOA"
    case TokenSecret = "UZ2_k9pC-lTB-IUV_Q-2Wyc7p3Q"
}

enum YelpURLConstructor: String {
    case baseURL = "https://api.yelp.com/v2/search/?term="
    case location = "&location="
    case suffix = "&limit=1"
}
 
let yelpBaseURL = "https://api.yelp.com/v2/search/?term=Bayside%20BMW&location=New%20York&limit=1"

let yelpCallbackURL = NSURL(string: "")

enum yelpImageModel: String {
    case name
    case image
}

// MARK: Strings

let circleImage = "blueCircularProgress"

let CD_LOCATION_STORE = "location"

let VehicleLocationViewReuseIdentifier = "vehicleLocationViewReuseIdentifier"

let serviceCenterLocationViewReuseIdentifier = "serviceCenterLocationViewReuseIdentifier"

/// Time format for server. Example: **"2/11/2016, 9:54:29 PM"**.
let serverTimeFormat = "M/dd/yyyy, H:mm:ss a"

// MARK: Numbers

/// The wait time (in seconds) to check against refreshing vehicle data.
let refreshWaitTime = Int(30 * 60)

// MARK: Endpoints

let vehicleListEndPoint = "http://cl.ly/e9z2/VehicleList.json"

// MARK: Vehicle Endpoints

enum VehicleEndpoint: String {
    case testingURL = "http://toohotz.noip.me:3000"
    case cloudTestURL = "http://cl.ly/e9z2/VehicleList.json"
    case RetrieveAllVehicles = "/vehicles"
    case RetrieveAllLocations = "/vehicle/Locations"
    case RetrieveFirstVehicle = "/vehicles/firstVehicle"
    case UserBaseURL = "/users"
}
