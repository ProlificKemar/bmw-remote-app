//
//  User.swift
//  TestingSwift
//
//  Created by Kemar White on 2/15/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {

    var username = ""
    var password = 0

    required init?(_ map: Map) { }

    init(username: String, password: Int)
    {
        self.username = username
        self.password = password
    }

    func mapping(map: Map) {
        username <- map["username"]
        password <- map["password"]
    }
}