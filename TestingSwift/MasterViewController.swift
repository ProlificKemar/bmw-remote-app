//
//  MasterViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 10/20/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackgroundUI()
    }

    private func setupBackgroundUI()
    {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bmw_grille")?.drawInRect(self.view.bounds)
        let bgImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        let bgBlurredImage = bgImage.applyBlurWithRadius(5.0, tintColor: nil, saturationDeltaFactor: 2)
        self.view.backgroundColor = UIColor(patternImage: bgBlurredImage!)
    }

    class func setupTableViewCellUI(reuseIdentifier: String) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: reuseIdentifier)
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.font = UIFont(name: "Avenir Next", size: 15)
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.detailTextLabel?.font = cell.textLabel?.font
        cell.textLabel?.textColor = Theme(rawValue: Theme.Electric.rawValue)?.color

        return cell
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return UIStatusBarStyle.LightContent
    }
}
