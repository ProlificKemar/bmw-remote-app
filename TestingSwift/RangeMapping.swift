//
//  RangeMapping.swift
//  TestingSwift
//
//  Created by Kemar White on 11/3/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation

struct RangeMapping {

    var inputValue = 0
    var outputScaleMin = 0
    var outputScaleMax = 0
    var inputScaleMin = 0
    var inputScaleMax = 0

    var mappedValue: Int {
        return inputScaleMin + (inputScaleMax - inputScaleMin) * (inputValue - outputScaleMin) / (outputScaleMax - outputScaleMin)
    }
}