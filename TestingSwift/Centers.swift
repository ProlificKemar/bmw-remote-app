//
//  Centers.swift
//  TestingSwift
//
//  Created by Kemar White on 11/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import CoreLocation

struct AuthorizedCenter: AuthorizedServiceCenter {
    let name: String
    let location: CLLocationCoordinate2D
    let number: String
    let url: NSURL
    let locationDetails: [NSObject : AnyObject]?
}

struct IndependentCenter: UnAuthorizedServiceCenter {
    let location: CLLocationCoordinate2D
    var name: String
    var number: String?
    let locationDetails: [NSObject : AnyObject]?
}
