//
//  VehicleLocation.swift
//  TestingSwift
//
//  Created by Kemar White on 11/3/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import CoreLocation

class VehicleLocation: NSObject, TransformableObject {

    var keyForObject: String
    var location: CLLocationCoordinate2D
    var purgeable: Bool
    var addressDictionary: [NSObject : AnyObject]?
    var addressLiteral: String?
    var name: String?

    override init() {
        keyForObject = "vehicleLocation"
        location = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        addressDictionary = [:]
        purgeable = true
    }

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(keyForObject, forKey: "vehicleLocation")
        aCoder.encodeObject(location as? AnyObject, forKey: "location")
        aCoder.encodeObject(purgeable, forKey: "purgeable")
        aCoder.encodeObject(addressLiteral, forKey: "addressLiteral")
        aCoder.encodeObject(addressDictionary, forKey: "addressDictionary")
        aCoder.encodeObject(name, forKey: "name")
    }

    required init?(coder aDecoder: NSCoder) {
        keyForObject = aDecoder.decodeObjectForKey("vehicleLocation") as! String
        location = aDecoder.decodeObjectForKey("location") as! CLLocationCoordinate2D
        purgeable = aDecoder.decodeObjectForKey("purgeable") as! Bool
        addressLiteral = aDecoder.decodeObjectForKey("addressLiteral") as? String
        addressDictionary = aDecoder.decodeObjectForKey("addressDictionary") as? [NSObject : AnyObject]
        name = aDecoder.decodeObjectForKey("name") as? String
    }
}
