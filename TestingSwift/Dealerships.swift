//
//  Dealerships.swift
//  TestingSwift
//
//  Created by Kemar White on 5/25/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation
import MapKit

@objc(Dealership)

/* Required so that older Obj-C archiver will use without any namespacing 
http://stackoverflow.com/questions/29472935/cannot-decode-object-of-class
*/

class Dealership : NSObject, TransformableObject {

    enum transformKeys: String {
        case Name = "name"
        case PhoneNumber = "phoneNumber"
        case Placemark = "placemark"
        case Url = "url"
    }

    var keyForObject: String
    var name: String?
    var phoneNumber: String?
    var placemark: MKPlacemark?
    var url: NSURL?

    override init() {
        keyForObject = "dealershipObject"
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        keyForObject = aDecoder.decodeObjectForKey("dealershipObect") as! String
        name = aDecoder.decodeObjectForKey(transformKeys.Name.rawValue) as! String?
        phoneNumber = aDecoder.decodeObjectForKey(transformKeys.PhoneNumber.rawValue) as! String?
        placemark = aDecoder.decodeObjectForKey(transformKeys.Placemark.rawValue) as! MKPlacemark?
        url = aDecoder.decodeObjectForKey(transformKeys.Url.rawValue) as! NSURL?
    }

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(keyForObject, forKey: "dealershipObject")
        aCoder.encodeObject(name, forKey: transformKeys.Name.rawValue)
        aCoder.encodeObject(phoneNumber, forKey: transformKeys.PhoneNumber.rawValue)
        aCoder.encodeObject(placemark, forKey: transformKeys.Placemark.rawValue)
        aCoder.encodeObject(url, forKey: transformKeys.Url.rawValue)
    }
}
