//
//  SearchedServiceCenterError.swift
//  TestingSwift
//
//  Created by Kemar White on 1/4/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

enum SearchedServiceCenterError: ErrorType {
    case NoCentersFound
    case SearchFailed(String)
}