//
//  VehicleAnnotation.swift
//  TestingSwift
//
//  Created by Kemar White on 11/21/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import MapKit

class VehicleAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D

    init(title: String, currentVehicleLocation: VehicleLocation)
    {
        self.title = title
        self.subtitle = FetchedDataController.globalInstance.vehicle?.name
        self.coordinate = currentVehicleLocation.location

        super.init()
    }
}
