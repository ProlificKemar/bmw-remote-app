//
//  ContentScreenViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 10/18/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import CoreLocation
class ContentScreenViewController: UIViewController, VehicleLocationViewDelegate {

    var currentView: UIView {
        return statusViews[self.itemIndex]
    }
    var itemIndex: Int = 0

    var labelName = ""

    var statusViews = [UIView.fromNib() as ServiceStatusView, UIView.fromNib() as VehicleLocationView]

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedView()
    }

    private func selectedView()
    {

        // Set our button title for the VehicleLocationView within the ContentScreenViewController's subviews
        if currentView.dynamicType == VehicleLocationView.self {
            let vLocationView = currentView as! VehicleLocationView
            vLocationView.setupViewWtihDelegate(self)
            let vehicleTitle = (AppLaunch.isVehicleLocationStored) ? "Find Vehicle" : "Save Vehicle"
            if AppLaunch.isVehicleLocationStored == false {
                vLocationView.vehicleParkingButton.hidden = true
            }
            vLocationView.vehicleLocationButton.setTitle(vehicleTitle, forState: UIControlState.Normal)
        }
        self.view.addSubview(currentView)
        NibDisplayManager.displayViewForViewController(currentView, viewControllerView: self.view)
    }

    private func fullScreenConstraintsForView(constrainedView: UIView)
    {
        // Must make sure this guy is off as we are setting our constraints manually
        constrainedView.translatesAutoresizingMaskIntoConstraints = false

        let views = Dictionary(dictionaryLiteral: ("constrainedView", constrainedView))

        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[constrainedView]|", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: views)

        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[constrainedView]|", options: .AlignAllCenterY, metrics: nil, views: views)
        
        view.addConstraints(verticalConstraints)
        view.addConstraints(horizontalConstraints)
    }

    //MARK: - VehicleLocationViewDelegate Methods

    func vehicleLocationViewSaveStatus(saveStatus: Bool)
    {
            let vLocationView = currentView as! VehicleLocationView
            let vehicleTitle = (saveStatus) ? "Find Vehicle" : "Save Vehicle"
            vLocationView.vehicleLocationButton.setTitle(vehicleTitle, forState: UIControlState.Normal)
            vLocationView.vehicleParkingButton.hidden = !saveStatus
    }

    func vehicleLocationViewPurgeStatus(status: Bool)
    {
        let vLocationView = currentView as! VehicleLocationView
        if status == true {
            vLocationView.vehicleLocationButton.setTitle("Save Vehicle", forState: .Normal)
            vLocationView.vehicleParkingButton.hidden = status
        }
    }
}
