//
//  ScheduleAppointmentViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 10/11/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
 
struct ServiceCustomer {
    var name: String
    var phoneNumber: String
    var email: String?
    var vehicle: String
    var advisor: String?
}

class ScheduleAppointmentViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var serviceCenterLabel: UILabel!
    
    @IBOutlet weak var serviceCenterAddressLabel: UILabel!

    @IBOutlet weak var firstNameTextField: UITextField!

    @IBOutlet weak var lastNameTextField: UITextField!

    @IBOutlet weak var phoneNumberTextField: UITextField!

    @IBOutlet weak var emailTextField: UITextField!

    @IBOutlet weak var vehicleTextField: UITextField!

    @IBOutlet weak var advisorPickerView: UIPickerView!

    var customer = ServiceCustomer(name: "", phoneNumber: "", email: nil, vehicle: "", advisor: nil)

    var serviceCenter = Dealership()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.serviceCenterLabel.text = self.serviceCenter.name
        self.serviceCenterAddressLabel.text = self.serviceCenter.placemark?.thoroughfare
    }

    private func verifiedCustomer() -> Bool
    {
        return (self.customer.name.isEmpty && self.customer.phoneNumber.isEmpty && self.customer.vehicle.isEmpty) ? false : true
    }

    func setupAppointmentViewController(center: Dealership)
    {
        self.serviceCenter = center
    }
}
