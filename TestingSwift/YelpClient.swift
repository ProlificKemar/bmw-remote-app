//
//  YelpClient.swift
//  TestingSwift
//
//  Created by Kemar White on 12/27/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import OAuthSwift
import ObjectMapper
import Result
import SwiftyJSON

struct YelpClient: OAuth1enticatable {
    let consumerKey: String
    let consumerSecret: String
    let token: String
    let tokenSecret: String
    let city: String
    private var _searchURL = String()

    private var yelpSearchURL: String {
        get {
            return _searchURL
        }
        set(newValue) {
            _searchURL = "\(YelpURLConstructor.baseURL.rawValue)\(newValue.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)&\(YelpURLConstructor.location)=\(String(city).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)\(YelpURLConstructor.suffix.rawValue)"
        }
    }

    private func originalImageFromYelpURL(originalURL: String) -> String
    {
        // http://stackoverflow.com/questions/22000077/how-to-request-larger-images-from-yelp-api
        let searchRange = originalURL.rangeOfString("/ms")

        return originalURL.stringByReplacingCharactersInRange(searchRange!, withString: "/o")
    }

    init(consumerKey: String, consumerSecret: String, token: String, tokenSecret: String, city: String)
    {
        self.consumerKey = consumerKey
        self.consumerSecret = consumerSecret
        self.token = token
        self.tokenSecret = tokenSecret
        self.city = city
    }

    mutating func searchForBusinessLocation(locationName: String, imageResult: (Result<YelpImage, YelpSearchError>) -> () )
    {
        let secondKeys = OAuthSwiftClient(consumerKey: consumerKey, consumerSecret: consumerSecret, accessToken: token, accessTokenSecret: tokenSecret)
        yelpSearchURL = locationName
        secondKeys.get(yelpSearchURL, success: { (data, response) -> Void in
            let responseObject = try? NSJSONSerialization.JSONObjectWithData(data, options: [])
            let responseDict = responseObject as? Dictionary<String, AnyObject>
            let searchedObject = Mapper<SearchedBusiness>().map(responseDict)
            guard let urlImageExists = searchedObject?.imageURL else { return }

            let formattedURL = NSURL(string: self.originalImageFromYelpURL(urlImageExists.absoluteString))
            let dataTask = NSURLSession.sharedSession().dataTaskWithURL(formattedURL!, completionHandler: { (imageData, urlResponse, error) -> Void in
                //  Check for any server side error
                if (urlResponse as? NSHTTPURLResponse)?.statusCode == 404 || (urlResponse as? NSHTTPURLResponse)?.statusCode == 500 {
                    imageResult(.Failure(YelpSearchError.ServerSideError))
                    return
                }
                guard let dataExists = imageData else {
                    imageResult(.Failure(YelpSearchError.invalidImageData))
                    return
                }
                let image = UIImage(data: dataExists)
                let foundImage = YelpImage(name: searchedObject?.name, image: image)
                imageResult(.Success(foundImage))
            })
            dataTask.resume()

            // Yelp error handling

            }) { (error) -> Void in
                if error.code == 400 {
                    imageResult(.Failure(YelpSearchError.validationFailed))
                } else {
                    imageResult(.Failure(YelpSearchError.searchFailed(error.localizedDescription)))
                }
        }
    }
}