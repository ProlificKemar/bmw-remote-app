//
//  VehicleLocationManager.swift
//  TestingSwift
//
//  Created by Kemar White on 11/5/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import AddressBook
import CoreLocation
import Contacts
import MapKit

class VehicleLocationManager: LocationManager {

    var retrievingLocation: Bool {
        didSet(purgeStatus) {
            self.vehicleLocation?.purgeable = purgeStatus
        }
    }
    /// The vehicle's current location.
    static let globalInstance = VehicleLocationManager()
    
    var vehicleLocation: VehicleLocation? {
        if AppLaunch.isVehicleLocationStored {
            return BMWFacade.localDatabaseManager.loadVehicleLocationModel()
        } else {
            let curentVehicleLocation = VehicleLocation()
            if let coordinateExists = currentLocation?.coordinate {
                curentVehicleLocation.location = coordinateExists
                return curentVehicleLocation
            }
        }
        return nil
    }

    override init() {
        retrievingLocation = false
        super.init()
    }

    private func addressStringFromPlacemarkDictionary(addressDict: [NSObject : AnyObject]?) -> String?
    {
        guard let addressDictExists = addressDict else { return nil }
        var concatAddressString: String?
        if let addressArray: [String] = addressDictExists["FormattedAddressLines"] as? [String] {
            for (_, addressString) in addressArray.enumerate() {
                if let concatAddressExists = concatAddressString {
                    concatAddressString = "\(concatAddressExists) \(addressString)"
                } else {
                    concatAddressString = addressString
                }
            }
            return concatAddressString
        }
        return nil
    }

    private func formatServiceCenterPhoneNumber(phoneNumber: String) -> String
    {
        // Weird first two characters on phone number that has to be removed first
        let uCharRange = NSRange(location: 0, length: 2)
        let intermText = (phoneNumber as NSString).stringByReplacingCharactersInRange(uCharRange, withString: "")
        let text1 = intermText.stringByReplacingOccurrencesOfString("(", withString: "")
        let text2 = text1.stringByReplacingOccurrencesOfString(")", withString: "")
        let text3 = text2.stringByReplacingOccurrencesOfString("-", withString: "")
        let text4 = text3.stringByReplacingOccurrencesOfString(" ", withString: "")

        return text4
    }

    /**
     Save the user's vehicle location to be retrieved at a later time.

     - parameter longTerm: Boolean if set to true will store the vehicle's location indefinitely
     if not, location will be purged after a few hours.
     */
    func saveVehicleLocation(successfullySaved:(Bool) -> () )
    {
        let geocoder = CLGeocoder()
        guard let currentVehicleLocation = BMWFacade.vehicleLocationManager.vehicleLocation  else { return }

        geocoder.reverseGeocodeLocation(CLLocation(latitude: currentVehicleLocation.location.latitude, longitude: currentVehicleLocation.location.longitude)) { (vehiclePlacemark, error) -> Void in
            if error != nil {
                print("An error occurred reverse locating the given location - \(error!.localizedDescription)")
                return
            } else if vehiclePlacemark?.isEmpty == false && vehiclePlacemark?.first != nil {
                print("The address is \(self.addressStringFromPlacemarkDictionary(vehiclePlacemark!.first!.addressDictionary))")
                let vehicleLocationToSave = VehicleLocation()
                vehicleLocationToSave.location = currentVehicleLocation.location
                vehicleLocationToSave.name = FetchedDataController.globalInstance.vehicle?.name
                vehicleLocationToSave.addressDictionary = vehiclePlacemark!.first!.addressDictionary
                vehicleLocationToSave.addressLiteral = self.addressStringFromPlacemarkDictionary(vehiclePlacemark!.first!.addressDictionary)
                // Save location to DB
                BMWFacade.localDatabaseManager.createVehicleLocationModel(vehicleLocationToSave, saveStatus: { (status) -> () in
                    successfullySaved(status)
                })
            }
        }
    }

    func directionsToCurrentVehicleLocation()
    {
        guard let currentVehicleLocation = BMWFacade.vehicleLocationManager.vehicleLocation else {
            print("Could not find a location for the vehicle.. Cancelling request.")
            return
        }
        let vehiclePlace = MKPlacemark(coordinate: CLLocationCoordinate2DMake(currentVehicleLocation.location.latitude, currentVehicleLocation.location.longitude), addressDictionary: currentVehicleLocation.addressDictionary as? [String : AnyObject])
        let mapItem = MKMapItem(placemark: vehiclePlace)
        let mapOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking]
        MKMapItem.openMapsWithItems([mapItem], launchOptions: mapOptions)
    }

    func drivingDirectionsToMapPoint(serviceCenterLocation: MKPlacemark)
    {
        let mapItem = MKMapItem(placemark: serviceCenterLocation)
        let mapOptions: [String : AnyObject] = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsShowsTrafficKey : true]

        mapItem.openInMapsWithLaunchOptions(mapOptions)
    }

    func centerMapOnCurrentVehicleLocation(mapView: MKMapView, animated: Bool)
    {
        // Manually load in current vehicle location
        guard let vehicleName = FetchedDataController.globalInstance.vehicle?.name else { return }
        let formattedVehicleName = "My \(vehicleName)'s Location"
        // TODO: Need to make sure location is not nil or throw and handle error
        guard let vLocation = BMWFacade.vehicleLocationManager.vehicleLocation else {
            print("Could not find a location for the vehicle.. Cancelling request.")
            return
        }
        LocationManager.centerOnLocation(mapView, placemark: MKPlacemark(coordinate: (vLocation.location), addressDictionary: [CNPostalAddressStreetKey : formattedVehicleName]), animated: true)
        let annotation = VehicleAnnotation(title: "My Vehicle's Location", currentVehicleLocation: vLocation)

        mapView.addAnnotation(annotation)
    }

    func callServiceCenter(center: ServiceCenter)
    {
        if let verifiedCenter = center as? AuthorizedServiceCenter {
            // Transform number for telprompt
            UIApplication.sharedApplication().openURL(NSURL(string: "telprompt://\(formatServiceCenterPhoneNumber(verifiedCenter.number))")!)
        } else if let indyCenter = center as? IndependentCenter {
            // Check if number is valid first then try to call 
            guard let centerNumber = indyCenter.number else { return }
            UIApplication.sharedApplication().openURL(NSURL(string: "telprompt://\(formatServiceCenterPhoneNumber(centerNumber))")!)
        }
    }
}
