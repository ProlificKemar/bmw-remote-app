//
//  LoginStatusError.swift
//  TestingSwift
//
//  Created by Kemar White on 2/15/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

enum LoginStatusError: ErrorType {
    case InvalidUsername
    case InvalidPassword
    case Unknown(String)
}

enum LoginStatusCode: Int {
    case InvalidUsername = 27
    case InvalidPassword = 88
    case Successful = 200
    case Unknown = 9000
}