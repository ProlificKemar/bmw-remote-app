//
//  NSDateExtension.swift
//  TestingSwift
//
//  Created by Kemar White on 3/24/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

extension NSDate {

    /**
     Formats a given date to be displayed as **Feb 3, 2020, 3:24 PM**.

     - parameter date: The date to be formatted.

     - returns: The formatted date parsed as a string.
     */
    class func shortenedDateAndTimeFormat(date: NSDate) -> String
    {
        let df = NSDateFormatter()
        df.dateStyle = .MediumStyle
        df.timeStyle = .ShortStyle

        return df.stringFromDate(date)
    }
}