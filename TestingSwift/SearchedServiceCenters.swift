//
//  SearchedServiceCenters.swift
//  TestingSwift
//
//  Created by Kemar White on 1/4/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import MapKit.MKPointAnnotation

struct SearchedServiceCenters {

    let centers: [ServiceCenter]
    let pins: [MKPointAnnotation]
}