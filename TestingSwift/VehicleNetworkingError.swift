//
//  VehicleNetworkingError.swift
//  TestingSwift
//
//  Created by Kemar White on 2/4/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

enum VehicleNetworkingError: ErrorType {
    case CannotParseElectricVehicle
    case CannotParseRegularVehicle
    case CannotParseVehicle
    case NoVehiclesFound
    case UnknownNetworkError(String)
}