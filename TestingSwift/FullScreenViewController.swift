//
//  FullScreenViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 10/16/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit

class FullScreenViewController: UIViewController, UIPageViewControllerDataSource {

    private var pageViewController: UIPageViewController?

    private var modelViewControllers: [UIViewController]!

    override func viewDidLoad() {
        super.viewDidLoad()
        modelViewControllers = pageViewVCs()
//        setupPageControl()
        reset()
    }

    private func pageViewVCs() -> [UIViewController]
    {
        let firstVC = self.storyboard?.instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.contentScreenViewController.rawValue) as! ContentScreenViewController
        let secondVC = self.storyboard?.instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.contentScreenViewController.rawValue) as! ContentScreenViewController

        return [firstVC, secondVC]
    }

    private func setupPageControl() {
        self.navigationController?.navigationBarHidden = false
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.darkGrayColor()
    }

    private func reset()
    {
       let pageController = self.storyboard?.instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.vehiclePagingViewController.rawValue) as! UIPageViewController
        pageController.dataSource = self
        let firstController = fullScreenViewForIndex(0)!
        let startingVCs = [firstController]
        pageController.setViewControllers(startingVCs, direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        self.pageViewController = pageController
        addChildViewController(self.pageViewController!)
        self.view.addSubview(self.pageViewController!.view)
        self.pageViewController!.didMoveToParentViewController(self)
    }

    private func fullScreenViewForIndex(index: Int) -> ContentScreenViewController?
    {
        if index < modelViewControllers.count {
            let contentItemController = self.storyboard?.instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.contentScreenViewController.rawValue) as! ContentScreenViewController
            contentItemController.itemIndex = index
            return contentItemController
        }
        return nil
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        let itemController = viewController as! ContentScreenViewController
        if itemController.itemIndex > 0 {
            return fullScreenViewForIndex(itemController.itemIndex - 1)
        }
        return nil
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {

        let itemController = viewController as! ContentScreenViewController
        if itemController.itemIndex < modelViewControllers.count {
            return fullScreenViewForIndex(itemController.itemIndex + 1)
        }
       return nil
    }

    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return modelViewControllers.count
    }

    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
