//
//  BMWVehicles.swift
//  TestingSwift
//
//  Created by Kemar White on 12/2/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import ObjectMapper

class BMWVehicle: Mappable, BMW {
    var name: String = ""
    var odometer: Int = 0
    var nextServiceMileage: Int = 0
    var VIN: String = ""
    var manufacturedYear: Int = 0
    var lastUpdated: String = ""

    required init?(_ map: Map) { }
    
    init(name: String, odometer: Int, nextServiceMileage: Int, vehicleVIN: String, vehicleManufacturedYear: Int, lastUpdated: String)
    {
        self.name = name
        self.odometer = odometer
        self.nextServiceMileage = nextServiceMileage
        self.VIN = vehicleVIN
        self.manufacturedYear = vehicleManufacturedYear
        self.lastUpdated = lastUpdated
    }

    init () { }

    func mapping(map: Map) {
        name <- map[VehicleDataModel.name.rawValue]
        odometer <- map[VehicleDataModel.odometerReading.rawValue]
        nextServiceMileage <- map[VehicleDataModel.nextServiceMileage.rawValue]
        VIN <- map[VehicleDataModel.VIN.rawValue]
        manufacturedYear <- map[VehicleDataModel.MY.rawValue]
        lastUpdated <- map[VehicleDataModel.lastModified.rawValue]
    }
}

class RegularVehicle: BMWVehicle, RegularCar {
    var fuelRange: Int = 0
    var fuelConsumption: Int = 0

    init(name: String, odometer: Int, nextServiceMileage: Int, vehicleVIN: String, vehicleManufacturedYear: Int, fuelRange: Int, fuelConsumption: Int, lastUpdated: String) {
        self.fuelRange = fuelRange
        self.fuelConsumption = fuelConsumption
        super.init(name: name, odometer: odometer, nextServiceMileage: nextServiceMileage, vehicleVIN: vehicleVIN, vehicleManufacturedYear: vehicleManufacturedYear, lastUpdated: lastUpdated)
    }

    override init() {
        super.init()
    }

    required init?(_ map: Map) {
        super.init(map)
    }

    override func mapping(map: Map) {
        super.mapping(map)
        fuelRange <- map[VehicleDataModel.fuelRange.rawValue]
        fuelConsumption <- map[VehicleDataModel.fuelLevel.rawValue]
    }

    func propertiesAreValid(superObject: BMWVehicle) -> Bool
    {
        // A little Swift reflection here to iterate through the class (and superclass) properties to 
        // check for nil values since the properties are optional
        let someMirror = Mirror(reflecting: superObject).superclassMirror()
        guard let superMirror = someMirror else {
            print("Superclass mirror does not exist.")
            return false
        }
        superMirror.children.forEach { (child) -> () in
            print(child.value)
        }
        return true
    }
}

class ElectricVehicle: BMWVehicle, ElectricCar {
    var electricRange: Int = 0
    var totalRange: Int = 0
    var electricConsumption: Int = 0
    var electricCharge: Int = 0

    init(name: String, odometer: Int, nextServiceMileage: Int, vehicleVIN: String, vehicleManufacturedYear: Int, electricRange: Int, totalRange: Int, electricConsumption: Int, electricCharge: Int, lastUpdated: String) {
        self.electricCharge = electricCharge
        self.electricConsumption = electricConsumption
        self.electricRange = electricRange
        self.totalRange = totalRange
        super.init(name: name, odometer: odometer, nextServiceMileage: nextServiceMileage, vehicleVIN: vehicleVIN, vehicleManufacturedYear: vehicleManufacturedYear, lastUpdated: lastUpdated)
    }

    override init() {
        super.init()
    }
    required init?(_ map: Map) {
        super.init(map)
    }

    override func mapping(map: Map) {
        super.mapping(map)
        electricRange <- map[VehicleDataModel.electricRange.rawValue]
        totalRange <- map[VehicleDataModel.electricRange.rawValue]
        electricConsumption <- map[VehicleDataModel.eletricConsumption.rawValue]
        electricCharge <- map[VehicleDataModel.electricCharge.rawValue]
    }
}
