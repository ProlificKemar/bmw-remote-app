//
//  VehicleTransformer.swift
//  TestingSwift
//
//  Created by Kemar White on 2/8/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import Result

struct VehicleTransformer {

    /**
     Validation for a regular vehicle's underlying data.

     - parameter vehicle: Vehicle to validate.

     - returns: A **Result** containing either a success of **RegularVehicle** or a **VehicleTransformerError**.
     */
    func validateRegularVehicle(vehicle: RegularVehicle) -> Result<RegularVehicle, VehicleTransformerError>
    {
        let rVehicle = reflectedVehicle(vehicle)
        switch rVehicle {
        case .Success(let validVehicle):
            if let stringError = isVehicleDataValid(vehicle) {
                return .Failure(VehicleTransformerError.TransformFailed(stringError))
            }
            let parsedVehicle = RegularVehicle(name: validVehicle.name, odometer: validVehicle.odometer, nextServiceMileage: validVehicle.nextServiceMileage, vehicleVIN: validVehicle.VIN, vehicleManufacturedYear: validVehicle.manufacturedYear, fuelRange: vehicle.fuelRange, fuelConsumption: vehicle.fuelConsumption, lastUpdated: validVehicle.lastUpdated)

            return .Success(parsedVehicle)

        case .Failure(let VehicleTransformerError.TransformFailed(errorString)):
            return .Failure(VehicleTransformerError.TransformFailed(errorString))
        }
    }

    /**
     Validation for an electric vehicle's underlying data.

     - parameter vehicle: Vehicle to validate.

     - returns: A **Result** containing either a success of **ElectricVehicle** or a **VehicleTransformerError**.
     */
    func validateElectricVehicle(vehicle: ElectricVehicle) -> Result<ElectricVehicle, VehicleTransformerError>
    {
        let eVehicle = reflectedVehicle(vehicle)
        switch eVehicle {
        case .Success(let validVehicle):
            if let stringError = isVehicleDataValid(vehicle) {
                return .Failure(VehicleTransformerError.TransformFailed(stringError))
            }
            let parsedVehicle = ElectricVehicle(name: validVehicle.name, odometer: validVehicle.odometer, nextServiceMileage: validVehicle.nextServiceMileage, vehicleVIN: validVehicle.VIN, vehicleManufacturedYear: validVehicle.manufacturedYear, electricRange: vehicle.electricRange, totalRange: vehicle.totalRange, electricConsumption: vehicle.electricConsumption, electricCharge: vehicle.electricCharge, lastUpdated: validVehicle.lastUpdated)

            return .Success(parsedVehicle)

        case .Failure(let VehicleTransformerError.TransformFailed(errorString)):
            return .Failure(VehicleTransformerError.TransformFailed(errorString))
        }
    }

    private func isVehicleDataValid(vehicle: BMWVehicle) -> String?
    {
        var stringVal: String?
        let mirrorVehicle = Mirror(reflecting: vehicle)

        for _ in mirrorVehicle.children.enumerate() {
            // Check to make sure no dummy values are going to be parsed
            mirrorVehicle.children.forEach({ (child) -> () in
                if (child.value as? String)?.isEmpty == true || (child.value as? Int) == 0 {
                    stringVal = child.label
                }
            })
        }
        return stringVal
    }

    private func reflectedVehicle(vehicle: BMWVehicle) -> Result<BMWVehicle, VehicleTransformerError>
    {
        // Here we use reflection on the underlying vehicle's superclass
        // where we will then validate to the desired vehicle type
        guard let mirrorVehicle = Mirror(reflecting: vehicle).superclassMirror() else {
            return .Failure(VehicleTransformerError.TransformFailed("Failed to mirror the vehicle."))
        }
        let reflectedVehicle = BMWVehicle()
        // Enumerate through the properties
        var isValid = false
        var emptyKey: String?
        for child in mirrorVehicle.children.enumerate() {

            // Check to make sure no dummy values are going to be parsed
            mirrorVehicle.children.forEach({ (child) -> () in
                if (child.value as? String)?.isEmpty == true || (child.value as? Int) == 0 {
                    emptyKey = child.label
                    return
                }
                isValid = true
            })
            if isValid == false {
                return .Failure(VehicleTransformerError.TransformFailed("Failed to transform due to an empty key - \(emptyKey!)"))
            }

            switch child.element.label {
            case .Some(VehicleDataModel.name.rawValue):
                guard let name = child.element.value as? String else {
                    return .Failure(VehicleTransformerError.TransformFailed("Failed to transform property - \(VehicleDataModel.name.rawValue)"))
                }
                reflectedVehicle.name = name
            case .Some(VehicleDataModel.VIN.rawValue):
                guard let VIN = child.element.value as? String else {
                    return .Failure(VehicleTransformerError.TransformFailed("Failed to transform property - \(VehicleDataModel.VIN.rawValue)"))
                }
                reflectedVehicle.VIN = VIN
            case .Some("manufacturedYear"):
                guard let MY = child.element.value as? Int else {
                    return .Failure(VehicleTransformerError.TransformFailed("Failed to transform property - \(VehicleDataModel.MY.rawValue)"))
                }
                reflectedVehicle.manufacturedYear = MY
            case .Some("odometer"):
                guard let odometerReading = child.element.value as? Int else {
                    return .Failure(VehicleTransformerError.TransformFailed("Failed to transform property - \(VehicleDataModel.odometerReading.rawValue)"))
                }
                reflectedVehicle.odometer = odometerReading

            case .Some(VehicleDataModel.nextServiceMileage.rawValue):
                guard let nextServicing = child.element.value as? Int else {
                    return .Failure(VehicleTransformerError.TransformFailed("Failed to transform property - \(VehicleDataModel.nextServiceMileage.rawValue)"))
                }
                reflectedVehicle.nextServiceMileage = nextServicing
            case .Some("lastUpdated"):
                guard let lastUpdated = child.element.value as? String else {
                    return .Failure(VehicleTransformerError.TransformFailed("Failed to transform property - \(VehicleDataModel.lastModified.rawValue)"))
                }
                reflectedVehicle.lastUpdated = lastUpdated
                return .Success(reflectedVehicle)
            default:
                break
            }
        }
        return .Failure(VehicleTransformerError.TransformFailed("Failed to transform root vehicle."))
    }
}