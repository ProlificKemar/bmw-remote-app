//
//  VehicleLocationView.swift
//  TestingSwift
//
//  Created by Kemar White on 11/3/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import MapKit

class VehicleLocationView: UIView, MKMapViewDelegate {

    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var mapView: MKMapView!

    @IBOutlet weak var vehicleLocationButton: UIButton!

    @IBOutlet weak var vehicleParkingButton: UIButton!

    var locationViewDelegate: VehicleLocationViewDelegate?

    func setupViewWtihDelegate(delegate: VehicleLocationViewDelegate)
    {
        self.locationViewDelegate = delegate
        self.mapView.delegate = self
        if AppLaunch.isVehicleLocationStored {BMWFacade.vehicleLocationManager.centerMapOnCurrentVehicleLocation(self.mapView, animated: true)}
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .Follow
    }

    @IBAction func saveVehicleLocation(sender: UIButton) {
        if AppLaunch.isVehicleLocationStored && BMWFacade.localDatabaseManager.vehicleLocationCount != 0 {
            let currentVehicleLocation = BMWFacade.localDatabaseManager.vehicle
            self.locationViewDelegate?.vehicleLocationViewSaveStatus(currentVehicleLocation != nil)
            BMWFacade.vehicleLocationManager.directionsToCurrentVehicleLocation()
        } else {
            BMWFacade.vehicleLocationManager.saveVehicleLocation({ (saveStatus) -> () in
                if AppLaunch.isVehicleLocationStored { BMWFacade.vehicleLocationManager.centerMapOnCurrentVehicleLocation(self.mapView, animated: true) }
                self.locationViewDelegate?.vehicleLocationViewSaveStatus(saveStatus)
            })
        }
    }

    @IBAction func finishParkingVehicle(sender: UIButton) {
        if BMWFacade.localDatabaseManager.vehicleLocationCount > 0 {
            BMWFacade.localDatabaseManager.purgePersistentStore(CoreDataSQLDatabase.VehicleLocationModel, deleteStatus: { (status) -> () in
                self.locationViewDelegate?.vehicleLocationViewPurgeStatus(status)
                if status == true {
                    self.mapView.removeAnnotations(self.mapView.annotations)
                }
            })
        } else {
            AppLaunch.setUserDefaultsBoolForKey(false, userDefaultsKey: AppLaunch.Keys.vehicleLocationKey)
        }
    }

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            // Draw regular blue location dot
            return nil
        }
        var vehicleAnnotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(VehicleAnnotationViewIdentifier.vehicleLocationView.rawValue) as? VehicleAnnotationView
        if vehicleAnnotationView == nil {
            vehicleAnnotationView = VehicleAnnotationView(annotation: annotation, reuseIdentifier: VehicleAnnotationViewIdentifier.vehicleLocationView, viewType: AnnotationViewType.WalkingDirections, hasPhoneNumber: false)
        }
        return vehicleAnnotationView
    }

    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        if control == view.leftCalloutAccessoryView {
            if (view as? VehicleAnnotationView)?.viewType == AnnotationViewType.WalkingDirections {
                BMWFacade.vehicleLocationManager.directionsToCurrentVehicleLocation()
            }
        }
    }
}

protocol VehicleLocationViewDelegate {

    func vehicleLocationViewSaveStatus(saveStatus: Bool)

    func vehicleLocationViewPurgeStatus(status: Bool)
}
