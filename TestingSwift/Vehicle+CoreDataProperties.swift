//
//  Vehicle+CoreDataProperties.swift
//  TestingSwift
//
//  Created by Kemar White on 10/12/15.
//  Copyright © 2015 toohotz. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Vehicle {

    @NSManaged var vin: String?
    @NSManaged var odometer: NSNumber?
    @NSManaged var name: String?
    @NSManaged var fuelRange: NSNumber?
    @NSManaged var fuelLevel: NSNumber?
    @NSManaged var nextServiceMileage: NSNumber?
    @NSManaged var electricCharge: NSNumber?
    @NSManaged var electricConsumption: NSNumber?
    @NSManaged var electricRange: NSNumber?
    @NSManaged var totalElectricRange: NSNumber?
    @NSManaged var manufacturedYear: NSNumber?
    @NSManaged var lastUpdated: String?
}
