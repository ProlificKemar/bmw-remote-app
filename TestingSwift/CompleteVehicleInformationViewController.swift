//
//  CompleteVehicleInformationViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 10/16/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit

class CompleteVehicleInformationViewController: UIPageViewController, UIPageViewControllerDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        let index = pageViewController.viewControllers?.first?.view.tag
        let fullScreenVC = UIStoryboard(name: MainStoryboardIdentifiers.fullScreenViewController.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(ViewControllersIdentifiers.vehicleDetail.rawValue) as! FullScreenViewController

       print("\(index) is the currently selected page" )
        return fullScreenVC
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        let index = pageViewController.viewControllers?.first?.view.tag
        let fullScreenVC = UIStoryboard(name: MainStoryboardIdentifiers.fullScreenViewController.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(ViewControllersIdentifiers.vehicleDetail.rawValue) as! FullScreenViewController

         print("\(index) is the currently selected page" )
        return fullScreenVC
    }
}
