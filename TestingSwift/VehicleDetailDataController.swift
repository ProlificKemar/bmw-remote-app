//
//  VehicleDetailDataController.swift
//  TestingSwift
//
//  Created by Kemar White on 1/25/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import HTZNetworkingManager
import Result

class VehicleDetailDataController {

    func retrieveVehicle(retrievedResult: Result<BMWVehicle, VehicleNetworkingError> -> () )
    {
        let networkingFacade = VehicleFacade()
        networkingFacade.retrieveFirstVehicleFromList { (vehicleResponse) -> () in
            switch vehicleResponse {
            case .Success(let vehicle):
                // Parse to correct date format
                let correctDate = self.formattedDateFromServer(vehicle.lastUpdated)
                guard let validDate = correctDate else {
                    retrievedResult(.Failure(VehicleNetworkingError.CannotParseVehicle))
                    return
                }
                vehicle.lastUpdated = self.updateDateDisplay(validDate)
                //TODO: Update vehicle data with new last updated time
                retrievedResult(.Success(vehicle))
            case .Failure(let NetworkingError.ResponseError(responseString)):
                retrievedResult(.Failure(VehicleNetworkingError.UnknownNetworkError(responseString)))
            }
        }
    }

    // Temp function for local db retrieval
    func retrieveVehicleFromDatabase(shouldReloadManually manualReload: Bool, retrievedResult: Result<BMWVehicle, VehicleNetworkingError> -> () )
    {
        let networkingFacade = VehicleNetworkingFacade()
        networkingFacade.retrieveFirstVehicle(shouldReloadManually: manualReload) { (vehicleResponse) in
            switch vehicleResponse {
            case .Success(let vehicle):
                // Parse to correct date format
                let correctDate = self.formattedDateFromServer(vehicle.lastUpdated)
                guard let validDate = correctDate else {
                    retrievedResult(.Failure(VehicleNetworkingError.CannotParseVehicle))
                    return
                }
                vehicle.lastUpdated = self.updateDateDisplay(validDate)
                //TODO: Update vehicle data with new last updated time
                retrievedResult(.Success(vehicle))
            case .Failure(let VehicleNetworkingError.UnknownNetworkError(responseString)):
                retrievedResult(.Failure(VehicleNetworkingError.UnknownNetworkError(responseString)))
            default:
                retrievedResult(.Failure(VehicleNetworkingError.CannotParseVehicle))
            }
        }
    }

    func retrieveVehicleImage(vehicle: VehicleModel) -> UIImage
    {
        let photoManager = VehicleImagePhotoManager()
        return photoManager.imageForVehicle(vehicle)
    }

    private func formattedDateFromServer(serverDate: String) -> NSDate?
    {
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = serverTimeFormat
        formatter.locale = NSLocale.currentLocale()
        let formattedDate = formatter.dateFromString(serverDate)
        return formattedDate
    }

    private func updateDateDisplay(lastUpdatedDate: NSDate) -> String
    {
        let df = NSDateFormatter()
        df.dateStyle = .MediumStyle
        df.timeStyle = .ShortStyle

        if wasLastUpdateToday(lastUpdatedDate) == true {
            df.dateStyle = .NoStyle
            let dateString = df.stringFromDate(lastUpdatedDate)
            return "Last updated: Today, \(dateString)"
        } else {
            return "Last updated: \(df.stringFromDate(lastUpdatedDate))"
        }
    }

    private func wasLastUpdateToday(lastUpdateDate: NSDate) -> Bool
    {
        let cal = NSCalendar.currentCalendar()
        let lastUpdateComponents = cal.components([NSCalendarUnit.Era, .Day, .Year, .Month], fromDate: lastUpdateDate)
        let todayComponents = cal.components([NSCalendarUnit.Era, .Day, .Year, .Month], fromDate: NSDate())

        return lastUpdateComponents.day == todayComponents.day
    }
}
