//
//  ViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 5/25/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import UIKit
import CoreLocation
import CoreSpotlight
import LocalAuthentication
import MapKit
import Result
import HTZNetworkingManager

class ViewController: MasterViewController, UITextFieldDelegate, CLLocationManagerDelegate, MKMapViewDelegate, ServiceCenterTableViewControllerDelegate, ViewControllerDismissal {

    @IBOutlet weak var dealershipsTableView: UITableView!

    @IBOutlet weak var dealershipLocationMapView: MKMapView!

    @IBOutlet weak var stackView: UIStackView!

    @IBOutlet weak var searchButton: UIButton!

    var foundServiceCenters = [ServiceCenter]?()

    private var dataController: DealershipTableViewController!

    // MARK: - Initialization

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if dealershipLocationMapView.annotations.isEmpty == false {
        } else {
            self.stackView.arrangedSubviews[0].hidden = true
        }
        dispatch_once(&StaticDispatch.token) { () -> Void in
            self.navigationController?.navigationBarHidden = true
            self.setupUI()
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        BMWFacade.vehicleLocationManager.delegate = self
        setupForceTouchPreviewing()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if UserLoginFacade.userLoginSharedInstance.isLoggedIn == false {
            loginScreen()
        }
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        dataController = nil
    }

    //MARK: - Private Methods

    private func hideUIElements(shouldHide shouldHide: Bool)
    {
        if shouldHide == false {
            dealershipLocationMapView.alpha = 1
            stackView.arrangedSubviews[0].hidden = shouldHide
            searchButton.hidden = !shouldHide
        }
    }

    private func loginScreen()
    {
        let lgSB = UIStoryboard(name: StoryboardName.UserAuthentication.rawValue, bundle: nil)
        let loginVC = lgSB.instantiateViewControllerWithIdentifier(UserAuthenticationStoryBoardIdentifiers.LoginVIewController.rawValue) as! LoginViewController
        loginVC.setupDelegate(self)
        presentViewController(loginVC, animated: true, completion: nil)
    }

    private func setupUI()
    {
        dealershipLocationMapView.delegate = self
        self.tabBarItem = UITabBarItem(title: "Servicing", image: UIImage(imageLiteral: Icon.HighPriority.rawValue), tag: 0)

        // Setup tabbar controller views
        let detailVehicleVC = UIStoryboard(name: MainStoryboardIdentifiers.vehicleDetailStoryboardName.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(ViewControllersIdentifiers.vehicleDetail.rawValue) as! VehicleDetailViewController
        detailVehicleVC.tabBarItem = UITabBarItem(title: "Status", image: UIImage(imageLiteral: Icon.Expired.rawValue), tag: 1)
        let pageViewVC = UIStoryboard(name: MainStoryboardIdentifiers.name.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.fullScreenViewController.rawValue) as! FullScreenViewController
        pageViewVC.tabBarItem = UITabBarItem(title: "More Info", image: UIImage(imageLiteral: Icon.SUV.rawValue), tag: 2)

        self.tabBarController?.setViewControllers([self, detailVehicleVC, pageViewVC], animated: false)
    }

    private func setupUserLocation()
    {
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse {
            BMWFacade.vehicleLocationManager.requestLocation()
            print("Get LM location: \(BMWFacade.vehicleLocationManager.currentLocation)")
        }
    }

    private func setupForceTouchPreviewing()
    {
        if traitCollection.forceTouchCapability == .Available {
            // register for our peek and pop actions
            registerForPreviewingWithDelegate(self, sourceView: view)
        }  else {
            print("3D touch is not supported on this device")
        }
    }

    private func showAlertController(message: String)
    {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }

    private func authenticateUserWithTouchID()
    {
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error)
        {
            let reason = "Please authenticate with Touch ID"
            context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: { (succes, error) -> Void in
                if succes {
                    self.showAlertController("Touch ID Authentication Passed")
                } else {
                    self.showAlertController("Touch ID Authentication Failed")
                }
            })
        } else {
            showAlertController("Touch ID isn't available.")
        }
    }

    private func forceTouchAvailable() -> Bool
    {
        var isForceTouchAvailable = false

        if traitCollection.respondsToSelector(Selector("forceTouchCapability")) == true {
            isForceTouchAvailable = traitCollection.forceTouchCapability == UIForceTouchCapability.Available
        }
        return isForceTouchAvailable
    }

    private func searchedCenters()
    {
        BMWFacade.serviceCenterLocationManager.searchForServiceCenters("BMW") { (searchResults) -> () in
            switch searchResults {

            case let .Success(centersFound):
                self.dataController = DealershipTableViewController()
                self.dataController.setupWithDelegate(self, serviceCenterTableView: self.dealershipsTableView, setupServiceCenters: centersFound.centers)
                // Map out service center pin annotations
                for centerPin in centersFound.pins {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.dealershipLocationMapView.addAnnotation(centerPin)
                    })
                }
            case .Failure(SearchedServiceCenterError.NoCentersFound):
                print("No service centers were found")
            default:
                print("An error occurred retrieving the service centers.")
            }
        }
    }

    //MARK: - IBActions

    @IBAction func searchForNearbyCenters(sender: UIButton)
    {
        guard let _ = BMWFacade.serviceCenterLocationManager.currentLocation else {
            print("The user's current location is nil. Cancelling request.")
            return
        }
        if sender.hidden == false {
            UIView.animateWithDuration(0.75, animations: { () -> Void in
                self.hideUIElements(shouldHide: false)
                }, completion: { (_) -> Void in
                    self.dealershipLocationMapView.showsUserLocation = true
                    self.dealershipLocationMapView.userTrackingMode = MKUserTrackingMode.Follow
                    self.searchedCenters()
            })
        }
    }

    //MARK: - MKMapViewDelegate Methods

    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) { }

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            // Draw regular blue location dot
            return nil
        }
        var vehicleAnnotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(VehicleAnnotationViewIdentifier.vehicleLocationView.rawValue) as? VehicleAnnotationView
        if vehicleAnnotationView == nil {
    vehicleAnnotationView = VehicleAnnotationView(annotation: annotation, reuseIdentifier: VehicleAnnotationViewIdentifier.serviceCenterView, viewType: AnnotationViewType.ServiceCenterView, hasPhoneNumber: false)
        }
        return vehicleAnnotationView
    }

    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        if control == view.leftCalloutAccessoryView {
            for center in foundServiceCenters! {
                if center.name == view.annotation!.title! {
                    let detailVC = UIStoryboard(name: MainStoryboardIdentifiers.name.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.detailViewController.rawValue) as! DealershipDetailViewController
                    detailVC.setupView(center)
                    tabBarController?.presentViewController(detailVC, animated: true, completion: nil)
                }
            }
        }
    }

    //MARK: - CLLocationManagerDelegate Methods

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            manager.requestWhenInUseAuthorization()
        } else if status == .AuthorizedWhenInUse {
            setupUserLocation()
        }
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {}

    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("An error occurred with the location service \(error.localizedDescription)")
    }

    func serviceCenterTableViewControllerDidSelectCellAtIndex(indexPath: NSIndexPath, selectedServiceCenter: ServiceCenter)
    {
        // Push here to VC with dealership information
        // If force touch use preview or else present the modal
        let detailVC = UIStoryboard(name: MainStoryboardIdentifiers.name.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(MainStoryboardIdentifiers.detailViewController.rawValue) as! DealershipDetailViewController
        detailVC.setupView(selectedServiceCenter)
        tabBarController?.presentViewController(detailVC, animated: true, completion: nil)
    }

    func serviceCenterTableViewControllerDoesHaveAvailbleCenters(centers: [ServiceCenter])
    {
        foundServiceCenters = centers
    }

    //MARK: - Trait Collection Methods

    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?)
    {
        super.traitCollectionDidChange(previousTraitCollection)
    }

    // MARK: - Search API Methods

    override func restoreUserActivityState(activity: NSUserActivity)
    {
         let searchIdentifier = activity.userInfo?["kCSSearchableItemActivityIdentifier"] as? String
        if searchIdentifier ==  BMWFacade.localDatabaseManager.vehicle?.name {
            let detailVehicleVC = UIStoryboard(name: MainStoryboardIdentifiers.vehicleDetailStoryboardName.rawValue, bundle: nil).instantiateViewControllerWithIdentifier(ViewControllersIdentifiers.vehicleDetail.rawValue) as! VehicleDetailViewController
            presentViewController(detailVehicleVC, animated: true, completion: nil)
        }
    }

    func dismissViewController(viewController: UIViewController) {
        viewController.dismissViewControllerAnimated(true, completion: nil)
    }

    deinit {
        print("Main View was dealloc.")
    }
}
