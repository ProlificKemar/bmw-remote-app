//
//  VehicleLocationEntity+CoreDataProperties.swift
//  TestingSwift
//
//  Created by Kemar White on 12/4/15.
//  Copyright © 2015 toohotz. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension VehicleLocationEntity {

    @NSManaged var address: String?
    @NSManaged var addressDictionary: [NSObject : AnyObject]?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var name: String?
    @NSManaged var purgeable: Bool

}
