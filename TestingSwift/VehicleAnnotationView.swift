//
//  VehicleAnnotationView.swift
//  TestingSwift
//
//  Created by Kemar White on 11/23/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import MapKit
import UIKit

enum AnnotationViewType: Int {
    case DrivingDirections, WalkingDirections, PhoneCall, ServiceCenterView

    var iconButton: UIButton {
        switch self {
        case .DrivingDirections:
            let myButton = UIButton(type: UIButtonType.Custom)
            myButton.frame = CGRectMake(0, 0, 22, 22)
             myButton.setBackgroundImage(UIImage(named: Icon.Driving.rawValue), forState: UIControlState.Normal)
            return myButton
        case .WalkingDirections:
            let myButton = UIButton(type: UIButtonType.Custom)
            myButton.frame = CGRectMake(0, 0, 22, 22)
            myButton.setBackgroundImage(UIImage(named: Icon.Walking.rawValue), forState: UIControlState.Normal)
            return myButton
        case .PhoneCall:
            let myButton = UIButton(type: UIButtonType.Custom)
            myButton.frame = CGRectMake(0, 0, 18, 18)
            myButton.setBackgroundImage(UIImage(named: Icon.Phone.rawValue), forState: UIControlState.Normal)
            return myButton
        case .ServiceCenterView:
            let myButton = UIButton(type: .DetailDisclosure)
            return myButton
        }
    }
}

class VehicleAnnotationView: MKPinAnnotationView {

    let viewType: AnnotationViewType

    init(annotation: MKAnnotation?, reuseIdentifier: VehicleAnnotationViewIdentifier, viewType:AnnotationViewType, hasPhoneNumber: Bool) {
        self.viewType = viewType
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier.rawValue)
        canShowCallout = true
        enabled = true
        pinTintColor = Theme.Electric.color
        leftCalloutAccessoryView = viewType.iconButton

        switch reuseIdentifier {

        case .vehicleLocationView:
            pinTintColor = Theme.Electric.color
            rightCalloutAccessoryView = hasPhoneNumber ? AnnotationViewType.PhoneCall.iconButton : nil

        case .serviceCenterView:
            rightCalloutAccessoryView = nil

        case .independentCenterView:
            rightCalloutAccessoryView = hasPhoneNumber ? AnnotationViewType.PhoneCall.iconButton : nil
        }
    }

   override init(frame: CGRect) {
        self.viewType = AnnotationViewType.WalkingDirections
        super.init(frame: frame)
    }

   required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }
}
