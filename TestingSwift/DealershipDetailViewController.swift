//
//  DealershipDetailViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 7/28/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import MapKit

class DealershipDetailViewController: MasterViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!

    var serviceCenter: ServiceCenter?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.mapView.delegate = self
        guard let validServiceCenter = serviceCenter else { return }
        let locationPlacemark = MKPlacemark(coordinate: validServiceCenter.location, addressDictionary: validServiceCenter.locationDetails as? [String : AnyObject])
        LocationManager.centerOnLocation(mapView, placemark: locationPlacemark, animated: true)
        mapView.addAnnotation(pinAnnotationForPlacemark(locationPlacemark))
    }

    func setupView(center: ServiceCenter)
    {
        serviceCenter = center
    }

    @IBAction func dismissView(sender: UIButton)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }

    private func pinAnnotationForPlacemark(placemark: MKPlacemark) -> MKAnnotation
    {
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if placemark.thoroughfare != nil { annotation.subtitle = placemark.thoroughfare! }

        return annotation
    }

    //MARK: MKMapViewDelegate Methods

    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        guard let validServiceCenter = serviceCenter else { return }
        let selectedServiceCenter = MKPlacemark(coordinate: validServiceCenter.location, addressDictionary: validServiceCenter.locationDetails as? [String : AnyObject])
        if control == view.leftCalloutAccessoryView {
            BMWFacade.vehicleLocationManager.drivingDirectionsToMapPoint(selectedServiceCenter)
        } else if control == view.rightCalloutAccessoryView {
            BMWFacade.vehicleLocationManager.callServiceCenter(validServiceCenter)
        }
    }

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            // Draw regular blue location dot
            return nil
        }
        var vehicleAnnotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(VehicleAnnotationViewIdentifier.serviceCenterView.rawValue) as? VehicleAnnotationView
        if vehicleAnnotationView == nil {
            if serviceCenter is IndependentCenter  {
                if (serviceCenter as! IndependentCenter).number != nil {
                    vehicleAnnotationView = VehicleAnnotationView(annotation: annotation, reuseIdentifier: .independentCenterView, viewType: AnnotationViewType.DrivingDirections, hasPhoneNumber: true)
                } else {
                    vehicleAnnotationView = VehicleAnnotationView(annotation: annotation, reuseIdentifier: .independentCenterView, viewType: AnnotationViewType.DrivingDirections, hasPhoneNumber: false)
                }
            } else if serviceCenter is AuthorizedCenter {
                vehicleAnnotationView = VehicleAnnotationView(annotation: annotation, reuseIdentifier: .vehicleLocationView, viewType: AnnotationViewType.DrivingDirections, hasPhoneNumber: true)
            } else {
                vehicleAnnotationView = VehicleAnnotationView(annotation: annotation, reuseIdentifier: VehicleAnnotationViewIdentifier.serviceCenterView, viewType: AnnotationViewType.ServiceCenterView, hasPhoneNumber: false)
            }
        }
        return vehicleAnnotationView
    }
}

