//
//  DealershipTableViewController.swift
//  TestingSwift
//
//  Created by Kemar White on 7/26/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class DealershipTableViewController: NSObject, UITableViewDataSource, UITableViewDelegate {

    weak var tableView: UITableView!

    weak var delegate: ServiceCenterTableViewControllerDelegate?
    
    var serviceCenters: [ServiceCenter]?

    //MARK: ServiceCenterTableViewControllerDelegate Methods

    func setupWithDelegate(serviceCenterDelegate: ServiceCenterTableViewControllerDelegate, serviceCenterTableView: UITableView, setupServiceCenters: [ServiceCenter]?)
    {
        tableView = serviceCenterTableView
        delegate = serviceCenterDelegate
        serviceCenters = setupServiceCenters
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK: UITableViewDelegate Methods

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (serviceCenters != nil) ? serviceCenters!.count : 0
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("DealershipListTableViewCell", forIndexPath: indexPath) as! DealershipListTableViewCell
        cell.setupCellWithServiceCenter(serviceCenters?[indexPath.row])

        if let foundCenters = serviceCenters where delegate != nil {
            delegate?.serviceCenterTableViewControllerDoesHaveAvailbleCenters(foundCenters)
        }
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let foundCenters = serviceCenters where delegate != nil {
            delegate?.serviceCenterTableViewControllerDidSelectCellAtIndex(indexPath, selectedServiceCenter: foundCenters[indexPath.row])
        }
    }
}

protocol ServiceCenterTableViewControllerDelegate: class, NSObjectProtocol {

    func serviceCenterTableViewControllerDidSelectCellAtIndex(indexPath: NSIndexPath, selectedServiceCenter: ServiceCenter)

    func serviceCenterTableViewControllerDoesHaveAvailbleCenters(centers: [ServiceCenter])
}
