//
//  BasicVehicleDetails.swift
//  TestingSwift
//
//  Created by Kemar White on 11/2/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation

@objc(BasicVehicleDetails)

class BasicVehicleDetails: NSObject, TransformableObject {

    var keyForObject = "basicVehicleDetailsObject"
    let name: String
    let manufacturedYear: NSNumber
    let VIN: String
    let odometerReading: NSNumber
    let fuelLevel: NSNumber
    let fuelRange: NSNumber
    /// Number of miles left until next servicing is required.
    let nextServiceMileage: NSNumber

    init(name: String, manufacturedYear: NSNumber, VIN: String, odometerReading: NSNumber, fuelLevel: NSNumber, fuelRange: NSNumber, nextServiceMileage: NSNumber)
    {
        self.name = name
        self.manufacturedYear = manufacturedYear
        self.VIN = VIN
        self.odometerReading = odometerReading
        self.fuelLevel = fuelLevel
        self.fuelRange = fuelRange
        self.nextServiceMileage = nextServiceMileage
    }

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(keyForObject, forKey: "basicVehicleDetailsObject")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(manufacturedYear, forKey: "manufacturedYear")
        aCoder.encodeObject(VIN, forKey: "VIN")
        aCoder.encodeObject(odometerReading, forKey: "odometerReading")
        aCoder.encodeObject(fuelLevel, forKey: "fuelLevel")
        aCoder.encodeObject(fuelRange, forKey: "fuelRange")
        aCoder.encodeObject(nextServiceMileage, forKey: "nextServiceMileage")
    }

   required init?(coder aDecoder: NSCoder) {
    self.keyForObject = aDecoder.decodeObjectForKey("basicVehicleDetailsObject") as! String
    self.name = aDecoder.decodeObjectForKey("name") as! String
    self.manufacturedYear = aDecoder.decodeObjectForKey("manufacturedYear") as! NSNumber
    self.VIN = aDecoder.decodeObjectForKey("VIN") as! String
    self.odometerReading = aDecoder.decodeObjectForKey("odometerReading") as! NSNumber
    self.fuelLevel = aDecoder.decodeObjectForKey("fuelLevel") as! NSNumber
    self.fuelRange = aDecoder.decodeObjectForKey("fuelRange") as! NSNumber
    self.nextServiceMileage = aDecoder.decodeObjectForKey("nextServiceMileage") as! NSNumber
    }
}
