//
//  LoadingScreenManager.swift
//  TestingSwift
//
//  Created by Kemar White on 2/12/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation

struct LoadingScreenManager {

    private var hud: MBProgressHUD!

    mutating func displayLoadingScreen(title: String?, parentView: UIView)
    {
        guard let title = title else { return }
        hud = MBProgressHUD.showHUDAddedTo(parentView, animated: true)
        hud.label.text = title
        hud.label.font = UIFont(name: "Avenir Next", size: 13)
    }

    mutating func hideLoadingScreen()
    {
        guard (hud) != nil else {
            print("There is no HUD on screen to hide. Cancelling.")
            return
        }
        hud.hideAnimated(true)
        hud = nil
    }
}