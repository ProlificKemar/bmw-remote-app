//
//  UserActivitySearchManager.swift
//  TestingSwift
//
//  Created by Kemar White on 10/28/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import CoreSpotlight
import MobileCoreServices

class UserActivitySearchManager: AnyObject {

    var searchableItems = [CSSearchableItem]()

    var activity: NSUserActivity

    init()
    {
        activity = NSUserActivity(activityType: "com.toohotz.testingswift.search")
        activity.eligibleForHandoff = false
        activity.eligibleForSearch = true
    }

    private func setupSearchItems()
    {
        guard let searchablesVehicle = FetchedDataController.globalInstance.vehicle else { return }
        let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeItem as String)
        attributeSet.title = "My \(searchablesVehicle.name)"

        let manufacturedYearString = String(searchablesVehicle.manufacturedYear)
        attributeSet.contentDescription = "'\(manufacturedYearString) \(searchablesVehicle.name)"
        attributeSet.keywords = ["\(searchablesVehicle.name) \(searchablesVehicle.odometer) miles"]
        let item = CSSearchableItem(uniqueIdentifier: nil, domainIdentifier: nil, attributeSet: attributeSet)

        let vinAttributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeItem as String)
        vinAttributeSet.title = searchablesVehicle.VIN
        vinAttributeSet.contentDescription = "\(searchablesVehicle.name)'s VIN"
        vinAttributeSet.keywords = ["VIN"]
        let vinItem = CSSearchableItem(uniqueIdentifier: nil, domainIdentifier: nil, attributeSet: vinAttributeSet)

        CSSearchableIndex.defaultSearchableIndex().indexSearchableItems([item, vinItem]) { (error) -> Void in
            if error != nil {
                print("An error occurred with the searchable item - \(error!.localizedDescription)")
            }
        }
    }

    func setupUserActivity()
    {
        setupSearchItems()
        activity.userInfo = ["name" : (FetchedDataController.globalInstance.vehicle?.name)!,
            "VIN" : (FetchedDataController.globalInstance.vehicle?.VIN)!,
            "miles" : (FetchedDataController.globalInstance.vehicle?.odometer)!]
        activity.title = "My BMW"
        activity.keywords = Set(arrayLiteral: "My BMW", "My Vehicle", "My Car")
        activity.becomeCurrent()
    }
}
