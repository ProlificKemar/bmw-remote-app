//
//  AlertControllerManager.swift
//  TestingSwift
//
//  Created by Kemar White on 2/27/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import UIKit

class AlertControllerManager {

    class func displayAlert(message: LoginAlertControllerMessage, onScreenViewController: UIViewController)
    {
        var alertController: UIAlertController!
        let OKAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default) { (_) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController = UIAlertController(title: "Hey there", message: message.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(OKAction)
        if message == .InvalidPassword {
            let forgotPasswordAction = UIAlertAction(title: "Forgot Password", style: UIAlertActionStyle.Destructive) { (_) -> Void in
                // Display some forgot password option
            }
            alertController.addAction(forgotPasswordAction)
        }
        onScreenViewController.presentViewController(alertController, animated: true, completion: nil)
    }
}
