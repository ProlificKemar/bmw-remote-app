//
//  ReachabilityManager.swift
//  TestingSwift
//
//  Created by Kemar White on 1/31/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import Reachability

class ReachabilityManager {

    static let sharedInstance = ReachabilityManager(usingClosures: true)

    private let noConnectionView = UIView.fromNib() as NoConnectionScreenView
    
    private var reachability: Reachability?

    /**
     Initialize the connection manager to use closures. If set to false, **NSNotification** will be used.


     - parameter usingClosures: Bool to determine whether to use closures.

     - returns: Initialized connection manager instance.
     */
    init(usingClosures: Bool)
    {
        setupReachability("www.google.com", useClosures: usingClosures)
        startNotifier()
    }

    private func setupReachability(hostname: String?, useClosures: Bool)
    {
        do {
            let reach = try hostname == nil ? Reachability.reachabilityForInternetConnection() : Reachability(hostname: hostname!)
            reachability = reach
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            // Display No internet connection 
            print("Cannot connect to Internet")
            print(address)
            displayNoConnectionView()
            return
        } catch {}

        if useClosures == true {
            reachability?.whenReachable = { reachability in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.checkForNoConnectionView()
                })
            }
            reachability?.whenUnreachable = {reachability in
                dispatch_async(dispatch_get_main_queue(), {[unowned self] () -> Void in

                    self.displayNoConnectionView()
                })
            }
        } else {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ReachabilityManager.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        }
    }

    @objc private func reachabilityChanged(notification: NSNotification)
    {
        let reachability = notification.object as? Reachability
        guard let reach = reachability else { return }
        switch reach.currentReachabilityStatus {
        case .NotReachable:
            displayNoConnectionView()
        default:
            checkForNoConnectionView()
        }
    }

    private func startNotifier()
    {
        do {
            try reachability?.startNotifier()
        } catch {
            print("Cannot start reachbility notifier")
            return
        }
    }

    private func stopNotifier()
    {
        reachability?.stopNotifier()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        reachability = nil
    }

    private func displayNoConnectionView()
    {
        let tempDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let currentVC = (tempDelegate.window?.rootViewController as! UITabBarController).selectedViewController
        let referenceView = self.noConnectionView
        referenceView.translatesAutoresizingMaskIntoConstraints = false
        currentVC?.view.addSubview(referenceView)
        NibDisplayManager.displayViewForViewController(referenceView, viewControllerView: currentVC!.view)
        currentVC?.view.bringSubviewToFront(referenceView)
    }

    private func checkForNoConnectionView()
    {
        // If the No Connection View is already on screen remove it
        let tempDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let currentVC = (tempDelegate.window?.rootViewController as! UITabBarController).selectedViewController
        for foundView in currentVC!.view.subviews {
            if foundView.dynamicType == self.noConnectionView.dynamicType {
                foundView.removeFromSuperview()
            }
        }
    }
}