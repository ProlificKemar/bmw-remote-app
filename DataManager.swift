//
//  DataManager.swift
//  TestingSwift
//
//  Created by Kemar White on 10/19/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation

class DataManager {

   class func loadDataFromPlist(plistName: String) -> [AnyObject]?
    {
        let path = NSBundle.mainBundle().pathForResource(plistName, ofType: "plist")
        if let plistArray =  NSArray(contentsOfFile: path!) as? [AnyObject] {
            return plistArray
        }
        return nil
    }
}
