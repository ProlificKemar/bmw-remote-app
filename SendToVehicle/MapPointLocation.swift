//
//  MapPointLocation.swift
//  TestingSwift
//
//  Created by Kemar White on 12/16/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import CoreLocation

class MapPointLocationCooridnate: NSObject {
    let longitude: CLLocationDegrees
    let latitude: CLLocationDegrees

    init(longitude: CLLocationDegrees, latitude: CLLocationDegrees)
    {
        self.longitude = longitude
        self.latitude = latitude
    }
}