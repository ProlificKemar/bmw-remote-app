//
//  MapsVCardParser.swift
//  TestingSwift
//
//  Created by Kemar White on 12/14/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import Contacts
import CoreLocation

struct MapsVCardParser {

    let vCard: NSData

    let parsedContact: CNContact?

    lazy var mapString: String = {

        let mappedString =  NSString(data: self.vCard, encoding: NSUTF8StringEncoding)!
        return  mappedString as String
    }()

    lazy var addressLocation: CLLocationCoordinate2D = {

        var location = CLLocationCoordinate2D()
        let locationStartRange = self.mapString.rangeOfString("ll=")
        let locationEndRange = self.mapString.rangeOfString("&t")

        let locationBeginningIndex = locationStartRange!.endIndex
        let locationEndIndex = locationEndRange!.startIndex

        let rawLocationCoordinate = self.mapString.substringWithRange(Range(locationBeginningIndex..<locationEndIndex))
        let locationCoordinateString = rawLocationCoordinate.stringByReplacingOccurrencesOfString("\\,-", withString: " ")

        let spaceIndex = locationCoordinateString.rangeOfString(" ")
        let latitude = CLLocationDegrees(locationCoordinateString.substringToIndex(spaceIndex!.startIndex))!
        let longitude = CLLocationDegrees(locationCoordinateString.substringFromIndex(spaceIndex!.endIndex))!

        location = CLLocationCoordinate2DMake(latitude, longitude)

        return location
    }()

    init(vCard: NSData)
    {
        self.vCard = vCard
        do {
            parsedContact = try CNContactVCardSerialization.contactsWithData(self.vCard).first as? CNContact
        } catch {
            print("An error occurred trying to deserialize the VCard contact information")
            parsedContact = nil
        }
    }

    mutating func distanceAwayFromLocation(currentLocation: CLLocation) -> String
    {
        // Need a way to capture current location
        let pointLocation = CLLocation(latitude: addressLocation.latitude, longitude: addressLocation.longitude)

        let milesMultiplier = 0.00062137
        let rawDistance = currentLocation.distanceFromLocation(pointLocation)
        let distanceInMiles = rawDistance * milesMultiplier

        return String(distanceInMiles)
    }
}