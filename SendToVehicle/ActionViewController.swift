//
//  ActionViewController.swift
//  SendToVehicle
//
//  Created by Kemar White on 10/19/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import UIKit
import MobileCoreServices
import MapKit
import Contacts
import AddressBook

class ActionViewController: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var locationNameLabel: UILabel!

    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var bottomTextConstraint: NSLayoutConstraint!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.tintColor = UIColor.whiteColor()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Get the item[s] we're handling from the extension context.
        // For example, look for an image and place it into an image view.
        // Replace this with something appropriate for the type[s] your extension supports.
        for item: AnyObject in self.extensionContext!.inputItems {
            let inputItem = item as! NSExtensionItem
            for provider: AnyObject in inputItem.attachments! {
                let itemProvider = provider as! NSItemProvider
                if itemProvider.hasItemConformingToTypeIdentifier(kUTTypeText as String) {
                    itemProvider.loadItemForTypeIdentifier(kUTTypeText as String, options: nil, completionHandler: { (mapItem, error) -> Void in
                        if error == nil {
                            guard let mapData = mapItem as? NSData else { return }
                            self.setupTextLabelFromMapData(mapData)
                        }
                    })
                }
            }
        }
    }

    private func setupUI()
    {
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationBar.shadowImage = UIImage()
        self.view.bringSubviewToFront(self.navigationBar)
    }

    private func setupTextLabelFromMapData(mapData: NSData)
    {
        let parser = MapsVCardParser(vCard: mapData)
        if let contactExists = parser.parsedContact where contactExists.givenName != "Dropped Pin" {
            let addr = contactExists.postalAddresses.first?.value as? CNPostalAddress
            setImageFromLocationName(contactExists.givenName, city: (addr?.city)!)
            locationNameLabel.text = contactExists.givenName
            let postAddrFormatter = CNPostalAddressFormatter()
            postAddrFormatter.style = .MailingAddress
            addressLabel.text = postAddrFormatter.stringFromPostalAddress(addr!)
        }
    }

    func setImageFromLocationName(name: String, city: String)
    {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            if CGRectGetHeight(self.view.frame) > 480 {
                self.imageViewBottomConstraint.constant += (CGRectGetHeight(self.view.frame) - 480) / 5
            }
        }
        var yClient = YelpClient(consumerKey: YelpKeys.ConsumerKey.rawValue, consumerSecret: YelpKeys.ConsumerSecret.rawValue, token: YelpKeys.Token.rawValue, tokenSecret: YelpKeys.TokenSecret.rawValue, city: city)
        yClient.searchForBusinessLocation(name) { (imageResult) -> () in
            switch imageResult {
            case let .Success(imageFound):
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.imageView.image = self.roundedRoundedImage(self.scaleImageToSize(imageFound.image!), radius: CGRectGetHeight(self.imageView.layer.frame) / 2 - ((CGRectGetHeight(self.view.frame) - 480) / 9))
                    self.imageView.clipsToBounds = true
                    self.imageView.layer.cornerRadius = (CGRectGetHeight(self.imageView.layer.frame) / CGRectGetWidth(self.imageView.layer.frame)) / 5
                })

            // Error handling

            case let .Failure(YelpSearchError.searchFailed(name)):
                print("Failed searching for \(name)")
            case .Failure(YelpSearchError.validationFailed):
                print("An error occurred validating the Yelp API keys provided.")
            default:
                print("An unknown error has occurred.")
            }
        }
    }

    private func scaleImageToSize(originalImage: UIImage) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(self.imageView.frame.size, false, 0)
        originalImage.drawInRect(CGRectMake(0, 0, self.imageView.layer.frame.size.width, self.imageView.layer.frame.size.height))
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return finalImage
    }

    private func roundedRoundedImage(image: UIImage, radius: CGFloat) -> UIImage
    {
        let imageLayer = CALayer()
        imageLayer.frame = CGRectMake(0, 0, image.size.width, self.imageView.layer.frame.size.height)
        imageLayer.contents = image.CGImage

        imageLayer.masksToBounds = true
        imageLayer.cornerRadius = radius

        UIGraphicsBeginImageContext(image.size)
        imageLayer.renderInContext(UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return roundedImage
    }

    @IBAction func done() {
        // Return any edited content to the host app.
        // This template doesn't do anything, so we just echo the passed in items.
        self.extensionContext!.completeRequestReturningItems(self.extensionContext!.inputItems, completionHandler: nil)
    }
}
