![Version](https://img.shields.io/badge/version-1.0-yellow.svg)
[![License](https://img.shields.io/dub/l/vibe-d.svg)](https://opensource.org/licenses/MIT)
# MyBMW
___
Personal app demonstrating front and backend integration of BMW vehicle services including service center  locating, detailed vehicle information, and a vehicle last parked locator.

### Inspiration

> Inspiried by the ever fast paced moving tech industry in automotives along with the desire to bring to the auto industry, a tigher integration with the latest and greatest tech to bring to cars alike.  The choice of Bayerische Motoren Worke AG as my manufacturer of choice lies within my appreciation of all that the company represents.  Hopefully it will inspire the engineers on all fronts to not only move the mobile application into five star territory but to more importantly further the company in its future successes.

*~Feb 1st, 2016.*

### Design Patterns
___
##### Singleton & Facade Pattern
Within the app's frontend structure, most of the modeling is accomplished by utilizing managers as simple facade managers or more intricate facade managers backed by singleton instances that only require initializiation once per app lifecycle. The benefit of the Facade pattern is seen where View Controllers only see and access a single Facade being the *BMWFacade* which underneath is backed by many other facade singleton instances.  This reduces the code being scattered across View Controllers and allows the code to be more easily understood.
##### Protocol Oriented Pattern

Throughout the app, this design pattern can be found in instances such as the *BMW* protocol which is then used to differentiate between different types of BMWs in a horizontal fashion as opposed to a convoluted subclass vertical hierarchy. In other instances such as the *OAuth1enticatable* protocol which defines a set of rules that must be met for a struct or class to conform to satisfy its requirements.
##### Functional Programming Pattern
Just a touch of functional programming was used to dig through JSON data faster when content is burried dictionaries and arrays deep.

### App Screenshots
![screen](http://f.cl.ly/items/400X2S3e0T172w0W1c1q/Screen%20Shot%202016-03-25%20at%201.50.57%20AM.png)

*Login Screen*

![screen2](http://f.cl.ly/items/2d0T3V080c3F1u3k3i2E/Screen%20Shot%202016-03-25%20at%2012.26.37%20PM.png)

*Vehicle Detail Screen*

### Submodules
* [Alamofire](https://github.com/Alamofire/Alamofire) - HTTP networking library written in Swift.
* [Reachability](https://github.com/ashleymills/Reachability.swift) - Swift replacement for Apple's Reachability sample, re-written in Swift with closures.
* [OAuthSwift](https://github.com/OAuthSwift/OAuthSwift) - Swift based OAuth library for iOS and OSX.
* [HTZNetworkingManager](https://github.com/toohotz/HotzNetworkingManager) - Personal Alamofire built upon wrapper for faster CRUD API endpoint requests with *SwiftyJSON* and *Result* closure callback parsing.
* [Result](https://github.com/antitypical/Result) - Swift framework providing *Result<Value, Error>*.
* [ObjectMapper](https://github.com/Hearst-DD/ObjectMapper) - Framework written in Swift that makes it easy for you to convert your model objects (classes and structs) to and from JSON.
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - SwiftyJSON makes it easy to deal with JSON data in Swift.

### Todos

 - Add  service scheduling for authorized service centers
 - Rework Watch App

License
----

MIT

