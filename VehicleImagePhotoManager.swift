//
//  VehicleImagePhotoManager.swift
//  TestingSwift
//
//  Created by Kemar White on 1/28/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import UIKit.UIImage

struct VehicleImagePhotoManager {

    var imageCache = NSCache()

    func imageForVehicle(vehicle: VehicleModel) -> UIImage
    {
        var vehicleImage = UIImage()
        switch vehicle {
        default:
            if let cachedVehicleImage = imageCache.objectForKey(vehicle.rawValue) {
                vehicleImage = cachedVehicleImage as! UIImage
            } else {
                vehicleImage = UIImage(named: vehicle.rawValue)!
                imageCache.setObject(vehicleImage, forKey: vehicle.rawValue)
            }
        }
        return vehicleImage
    }
}