//
//  CloudAPITests.swift
//  TestingSwift
//
//  Created by Kemar White on 1/27/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import Foundation
import XCTest
import HTZNetworkingManager
import Result

@testable import TestingSwift

class CloudAPITests: XCTestCase {

    func testRetrieveAllVehicles()
    {
        let APIExpectation = self.expectationWithDescription("The retrieve vehicles API functions.")
        let nManager = VehicleNetworkingFacade()
        nManager.retrieveVehicles(false) { (vehicleResponse) in
            switch vehicleResponse {
            case .Success(let vehicles):
                XCTAssertFalse(vehicles.isEmpty, "API works but no vehicles were returned.")
            case .Failure(VehicleNetworkingError.NoVehiclesFound):
                XCTAssertTrue(true, "No vehicles are currently in the database")
            case .Failure(let VehicleNetworkingError.UnknownNetworkError(responseString)):
                XCTAssertTrue(responseString.isEmpty, "An error was received - \(responseString)")
            default:
                print("Some other case")
            }
            APIExpectation.fulfill()
        }
        waitForExpectationsWithTimeout(10) { (error) -> Void in
            if error != nil {
                print("\(error!.localizedDescription)")
            }
        }
    }

    func testRetrieveFirstVehicle()
    {
        let APIExpectation = self.expectationWithDescription("The retrieve vehicles API functions.")
        let nManager = VehicleNetworkingFacade()
        nManager.retrieveFirstVehicle(shouldReloadManually: false) { (vehicleResponse) in
            switch vehicleResponse {
            case .Success(let vehicle):
                print(vehicle.name)
            case .Failure(let VehicleNetworkingError.UnknownNetworkError(responseString)):
                XCTAssertTrue(responseString.isEmpty, "An error was received - \(responseString)")
            default:
                print("An unhandled error occurred.")
                return
            }
            APIExpectation.fulfill()
        }
        waitForExpectationsWithTimeout(10) { (error) -> Void in
            if error != nil {
                print("\(error!.localizedDescription)")
            }
        }
    }
}

class JSONCloudAPITests: XCTestCase {

    func testRetrieveAllVehicles()
    {
        let APIExpectation = self.expectationWithDescription("The retrieve vehicles API functions.")
        let nManager = VehicleFacade()
        nManager.retrieveVehicles { (vehicleResponse) -> () in
            switch vehicleResponse {
            case .Success(let vehicle):
                // Simple print of the first vehicle's name
                print(vehicle[0].name)
            case .Failure(let NetworkingError.ResponseError(responseString)):
                XCTAssertTrue(responseString.isEmpty, "An error was received - \(responseString)")
            }
            APIExpectation.fulfill()
        }
        waitForExpectationsWithTimeout(10) { (error) -> Void in
            if error != nil {
                print("\(error!.localizedDescription)")
            }
        }
    }

    func testRetrieveFirstVehicle()
    {
        let APIExpectation = self.expectationWithDescription("The retrieve first vehicle API functions.")
        let nManager = VehicleFacade()
        nManager.retrieveFirstVehicleFromList { (vehicleResponse) -> () in
            switch vehicleResponse {
            case .Success(let vehicle):
                // Simple print of the vehicle's name
                print(vehicle.name)
            case .Failure(let NetworkingError.ResponseError(responseString)):
                XCTAssertTrue(responseString.isEmpty, "An error was received - \(responseString)")
            }
            APIExpectation.fulfill()
        }
        waitForExpectationsWithTimeout(10) { (error) -> Void in
            if error != nil {
                print("\(error!.localizedDescription)")
            }
        }
    }

    func testLogin()
    {
        let APIExpectation = self.expectationWithDescription("Login User API")
        let loginFacade = UserLoginFacade()

        loginFacade.loginUser(User(username: "john.johnson@gmail.com", password: 1234)) { (loginResponse) -> () in
            switch loginResponse {
            case .Success(_):
                break
            case .Failure(LoginStatusError.InvalidUsername):
                XCTAssert(false, "An invalid username was provided")
            case .Failure(LoginStatusError.InvalidPassword):
                XCTAssert(false, "An invalid password was provided")
            case .Failure(let LoginStatusError.Unknown(errorString)):
                XCTAssert(false, "An unknown login error occurred. Failure reason - \(errorString)")
            }
            APIExpectation.fulfill()
        }
        waitForExpectationsWithTimeout(10) { (error) -> Void in
            if error != nil {
                print("\(error!.localizedDescription)")
            }
        }
    }
}
