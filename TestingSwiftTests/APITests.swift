//
//  APITests.swift
//  TestingSwift
//
//  Created by Kemar White on 9/2/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import XCTest
import CoreLocation
import HTZNetworkingManager
import Result

@testable import TestingSwift

class APITests: XCTestCase, CLLocationManagerDelegate {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testServiceCentersAPI()
    {
        let serviceCenterManager = DealershipLocationManager()
        let APIExpectation = self.expectationWithDescription("The service center API works \n ")
        serviceCenterManager.searchForDealership("BMW") { (_, foundCenters) -> () in
            XCTAssertNotNil(foundCenters)
            XCTAssertFalse(foundCenters!.isEmpty, "No service centers were found")
            for (index, serviceCenter) in foundCenters!.enumerate() {
                print("The (\(index)) service center is \(serviceCenter.name) \n")
            }
            APIExpectation.fulfill()
        }
        waitForExpectationsWithTimeout(10) { (error) -> Void in
            print("\(error?.localizedDescription)")
        }
    }
}
