//
//  CoreDataTests.swift
//  TestingSwift
//
//  Created by Kemar White on 11/17/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import XCTest
import CoreData
@testable import TestingSwift

class CoreDataTests: XCTestCase {

    private func createFiveEntities()
    {
        BMWFacade.localDatabaseManager.createVehicleLocationModel(VehicleLocation()) { (_) -> () in
        }

    }
    func testBackgroundHouseKeepoing()
    {
        for _ in 0...10
        {
            createFiveEntities()
        }
        measureBlock() {
            XCTAssertNotNil(BMWFacade.localDatabaseManager.loadVehicleLocationModelHouseKeepBackground())
        }
    }

    func testLoadingVehicleMainThread()
    {
        for _ in 0...10
        {
        createFiveEntities()
        }
        measureBlock { () -> Void in
            XCTAssertNotNil(BMWFacade.localDatabaseManager.loadVehicleLocationModel())
        }
    }
}
