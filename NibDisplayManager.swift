//
//  NibDisplayManager.swift
//  TestingSwift
//
//  Created by Kemar White on 2/1/16.
//  Copyright © 2016 toohotz. All rights reserved.
//

import UIKit

class NibDisplayManager {

    /**
     Displays a **UIView** centered on top of another view.

     - parameter displayView:        The view to display.
     - parameter viewControllerView: The superview for whom the displayView will be shown on top of.
     */
    class func displayViewForViewController(displayView: UIView, viewControllerView: UIView)
    {
        displayView.translatesAutoresizingMaskIntoConstraints = false

        let views = Dictionary(dictionaryLiteral: ("displayView", displayView))

        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[displayView]|", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: views)
         let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[displayView]|", options: .AlignAllCenterY, metrics: nil, views: views)

        viewControllerView.addConstraints(verticalConstraints)
        viewControllerView.addConstraints(horizontalConstraints)
    }
}