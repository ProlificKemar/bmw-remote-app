//
//  DetailInterfaceController.swift
//  TestingSwift
//`
//  Created by Kemar White on 9/6/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import WatchKit

class DetailInterfaceController: WKInterfaceController, CLLocationManagerDelegate {

    @IBOutlet var titleLabel: WKInterfaceLabel!
    
    @IBOutlet var phoneNumberLabel: WKInterfaceLabel!
    
    @IBOutlet var interfaceMap: WKInterfaceMap!

    override func awakeWithContext(context: AnyObject?) {
        if let serviceCenterDetails = context as? Dealership {
            if let title = serviceCenterDetails.name {
                titleLabel.setText(title)
            }
            if let placemark = serviceCenterDetails.placemark {
                centerLocation(placemark)
            }
            if let phonenumber = serviceCenterDetails.phoneNumber {
                phoneNumberLabel.setText(phonenumber)
            }
        }
    }

    private func centerLocation(placemark: CLPlacemark)
    {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(placemark.location!.coordinate, .defaultSpan, .defaultSpan)
        interfaceMap.setRegion(coordinateRegion)
        interfaceMap.addAnnotation(placemark.location!.coordinate, withPinColor: .Purple)
    }
}
