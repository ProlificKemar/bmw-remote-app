//
//  HTZWatchManager.swift
//  TestingSwift
//
//  Created by Kemar White on 8/28/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import WatchConnectivity

class HTZWatchSessionManager {
    let session: WCSession?

    init?(delegate: WCSessionDelegate)
    {
        #if os(iOS)
        session = (WCSession.isSupported() ) ? WCSession.defaultSession() : nil
        #elseif os(watchOS)
        session = WCSession.defaultSession()
        #endif
        if session != nil {
            self.session!.delegate = delegate
            session!.activateSession()
            #if os(iOS)
            print("WatchSession activated on device.", terminator: "\n")
            #elseif os(watchOS)
             print("WatchSession activated on Apple Watch.", terminator: "\n")
            #endif
        }
    }

    private func canInterfaceWithCounterpartDevice() -> Bool
    {
        #if os(iOS)
        return session!.paired && session!.watchAppInstalled
        #elseif os(watchOS)
        return session!.reachable && WCSession.isSupported()
        #endif
    }
}