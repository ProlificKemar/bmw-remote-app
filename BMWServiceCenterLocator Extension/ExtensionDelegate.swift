//
//  ExtensionDelegate.swift
//  BMWServiceCenterLocator Extension
//
//  Created by Kemar White on 7/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {

    func applicationDidFinishLaunching() {

        var session: WCSession!
        session = WCSession.defaultSession()
        session.delegate = self
        session.activateSession()

        let transfers = session.outstandingUserInfoTransfers
        if transfers.count > 0 {
            let transfer = transfers.first!
            transfer.cancel()
        }
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        // Check for user location changes and update closest service center if needed
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
}
