//
//  Extensions+CLLocation.swift
//  TestingSwift
//
//  Created by Kemar White on 9/6/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationDegrees {

    static var defaultSpan: Double {
        return 500
    }
}
