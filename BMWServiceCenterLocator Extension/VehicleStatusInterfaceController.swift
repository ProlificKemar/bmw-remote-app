//
//  VehicleStatusInterfaceController.swift
//  TestingSwift
//
//  Created by Kemar White on 11/2/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class VehicleStatusInterfaceController: WKInterfaceController, WCSessionDelegate {

    var watchSessionManager: HTZWatchSessionManager!

    var fuelRangeMapping = RangeMapping()

    @IBOutlet var fuelRangeCircleImage: WKInterfaceImage!
    
    @IBOutlet var vehicleNameLabel: WKInterfaceLabel!
    
    @IBOutlet var doorStatusLabel: WKInterfaceLabel!

    @IBOutlet var fuelRangeLabel: WKInterfaceLabel!


    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        setupSession()
        requestVehicleInformation()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    private func setupSession()
    {
        watchSessionManager = HTZWatchSessionManager(delegate: self)
    }

    private func requestVehicleInformation()
    {
        print("Sending Request to iPhone")
        let parameters = [MessageKey.RequestVehicleData.rawValue : "Hey there iPhone!"]
        if watchSessionManager.session != nil && watchSessionManager.session!.reachable {
            watchSessionManager.session!.sendMessage(parameters, replyHandler: { (replyDict) -> Void in
                guard let vehicleDict = replyDict[MessageKey.RequestVehicleData.rawValue] as? [String : AnyObject] else { return }
                // Decode vehicle model received
                let vehicle = BasicVehicleDetails.transformDictionaryIntoObject(vehicleDict) as BasicVehicleDetails

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.vehicleNameLabel.setText(vehicle.name)
                    self.fuelRangeLabel.setText("\(vehicle.fuelRange.stringValue)mi")
                    let mappedRange = RangeMapping(inputValue:500, outputScaleMin: 0, outputScaleMax: 530, inputScaleMin: 0, inputScaleMax: 100)
                    self.fuelRangeCircleImage.setImageNamed(circleImage)
                    self.fuelRangeCircleImage.startAnimatingWithImagesInRange(NSRange(location: 0, length: mappedRange.mappedValue - 1), duration: 1.5, repeatCount: 1)
                })
                }, errorHandler: { (error) -> Void in
                    print("An error occurred sending the message \(error.localizedDescription)")
            })
        }
    }

    //MARK: WCSessionDelegate Methods

    func session(session: WCSession, didReceiveMessage message: [String : AnyObject])
    {
        guard let vehicleDict = message[MessageKey.RequestVehicleData.rawValue] as? [String : AnyObject] else { return }
             let vehicle = BasicVehicleDetails.transformDictionaryIntoObject(vehicleDict) as BasicVehicleDetails
                print("The vehicle name is \(vehicle.name)")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.doorStatusLabel.setText(vehicle.name)
                })


    }
}
