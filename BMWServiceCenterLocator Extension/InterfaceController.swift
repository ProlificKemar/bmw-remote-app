//
//  InterfaceController.swift
//  BMWServiceCenterLocator Extension
//
//  Created by Kemar White on 7/30/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate, CLLocationManagerDelegate {

    @IBOutlet var interfaceTable: WKInterfaceTable!

    var watchSessionManager: HTZWatchSessionManager!

    let rowType = "tableRowController"

    var dealerships = [Dealership]()

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }

    override func willActivate() {
        super.willActivate()

        createWatchSession()
    }

   private func createWatchSession() {
        struct TokenHolder {
            static var token: dispatch_once_t = 0;
        }

        dispatch_once(&TokenHolder.token) {
            self.setupSession()
        }
    }

    private func setupSession()
    {
        watchSessionManager = HTZWatchSessionManager(delegate: self)
    }

    //MARK: WCSessionDelegate Methods

    func session(session: WCSession, didReceiveMessage message: [String : AnyObject])
    {
        dealerships = [Dealership]()
        if let listOfServiceCenters = message[MessageKey.Dealerships.rawValue] as? [ [String : AnyObject] ] {
            for encodedServiceCenter in listOfServiceCenters {
                let serviceCenter = Dealership.transformDictionaryIntoObject(encodedServiceCenter) as Dealership
                dealerships.append(serviceCenter)
            }
            interfaceTable.setNumberOfRows(dealerships.count, withRowType: rowType)

            for (index, serviceCenter) in dealerships.enumerate() {
                let row = interfaceTable.rowControllerAtIndex(index) as! TableRowDataModel
                row.textInterfaceLabel.setText(serviceCenter.name!)
            }
        }
    }

    @IBAction func requestServiceCenters()
    {
        let parameters = [MessageKey.Command.rawValue : MessageKey.Dealerships.rawValue]
        if watchSessionManager.session != nil && watchSessionManager.session!.reachable {
            watchSessionManager.session!.sendMessage(parameters, replyHandler: { (replyDict) -> Void in
                print("The reply dict holds \(replyDict)", terminator: "\n")
                }, errorHandler: { (error) -> Void in
                     print("An error occurred \(error.localizedDescription)", terminator: "\n")
            })
        }
    }
}
