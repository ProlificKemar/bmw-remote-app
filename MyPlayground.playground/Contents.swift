//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

var someName = "Can you hear me now"

if someName.rangeOfString("you") != nil {
    print("String found")
}

let sampleVCardString = "BEGIN:VCARD VERSION:3.0 PRODID:-//Apple Inc.//iOS 9.1//EN N:;San Jose BMW Motorcycles;;; FN:San Jose BMW Motorcycles item1.ADR;type=HOME;type=pref:;;1990 W San Carlos St;San Jose;CA;95128;United Statesitem1.X-ABLabel:Addressitem1.X-ABADR:USitem2.URL;type=pref:http://maps.apple.com/maps?address=1990%20W%20San%20Carlos%20St%20San%20Jose%20CA%2095128%20United%20States&ll=37.322956\\,-121.931387&t=m"

let nameStartRange = sampleVCardString.rangeOfString("FN:")
let nameEndRange = sampleVCardString.rangeOfString("item1")

let nameBeginningIndex = nameStartRange?.endIndex
let nameEndIndex = nameEndRange?.startIndex

let name = sampleVCardString.substringWithRange(Range<String.Index>(start: nameBeginningIndex!, end: nameEndIndex!))

let locationStartRange = sampleVCardString.rangeOfString("ll=")
let locationEndRange = sampleVCardString.rangeOfString("&t")

let locationBeginningIndex = locationStartRange!.endIndex
let locationEndIndex = locationEndRange!.startIndex

let unformattedLocationCoordinates = sampleVCardString.substringWithRange(Range<String.Index>(start: locationBeginningIndex, end: locationEndIndex))
let locationCooridnateString = unformattedLocationCoordinates.stringByReplacingOccurrencesOfString("\\,-", withString: " ")

let spaceIndex = locationCooridnateString.rangeOfString(" ")
let latitude = locationCooridnateString.substringToIndex(spaceIndex!.startIndex)
let longitude = locationCooridnateString.substringFromIndex(spaceIndex!.endIndex)

/*
BEGIN:VCARD
VERSION:3.0
PRODID:-//Apple Inc.//iOS 9.1//EN
N:;San Jose BMW Motorcycles;;;
FN:San Jose BMW Motorcycles
item1.ADR;type=HOME;type=pref:;;1990 W San Carlos St;San Jose;CA;95128;United States
item1.X-ABLabel:Address
item1.X-ABADR:US
item2.URL;type=pref:http://maps.apple.com/maps?address=1990%20W%20San%20Carlos%20St%20San%20Jose%20CA%2095128%20United%20States&ll=37.322956\,-121.931387&t=m
item2.X-ABLabel:map url
END:VCARD
*/

var _privateString = String()

var frontString: String {
    get {
        return _privateString
    }

    set(newValue) {
        _privateString = "\(newValue)cars"
    }
}

frontString = "hello"
print(_privateString)

let yelpBaseURL = "https://api.yelp.com/v2/search/?term=Bayside%20BMW&location=New%20York&limit=1"

let baseURL = "https://api.yelp.com/v2/search/?term="
let suffix = "&limit=1"

var _privateURL = String()

var formattedURL: String {
    get {
    return _privateURL
    }
    set(newValue) {
        _privateURL = "\(baseURL)\(newValue.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()))\(suffix)"
    }
}

formattedURL = "cats control dis world-U"
print(formattedURL)

var msString = "http://s3-media1.fl.yelpcdn.com/bphoto/nX08jc6lKA3daxx4kwMJIA/ms.jpg"
let searchRange = msString.rangeOfString("/ms")
let originalString = msString.stringByReplacingCharactersInRange(searchRange!, withString: "/o")