//
//  VehicleNetworkingManager.swift
//  TestingSwift
//
//  Created by Kemar White on 11/10/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation
import HTZNetworkingManager
import Result
import SwiftyJSON
import ObjectMapper

/// Webserver Facade
class VehicleNetworkingFacade: HTZNetworkingFacade {

    static let vehicleNetworkingSharedInstance = VehicleNetworkingFacade()

    var lastChecked: CFTimeInterval?

    override init() {
        super.init()
        self.networkingManager.baseURL  = VehicleEndpoint.testingURL.rawValue
    }

    private func shouldReloadData() -> Bool
    {
        guard let checkInTime = VehicleNetworkingFacade.vehicleNetworkingSharedInstance.lastChecked else { return true }
        let reloadCheckTime = CACurrentMediaTime()

        let duration = reloadCheckTime - checkInTime

        if Int(duration) > refreshWaitTime {
            // time to refresh data!
            return true
        }
        return false
    }

    func retrieveVehicles(manualReload: Bool, vehicleData: Result<[BMWVehicle], VehicleNetworkingError> -> () )
    {
        if manualReload == false {
            if shouldReloadData() == false { return }
        }
        self.networkingManager.getDataFromEndPoint(VehicleEndpoint.RetrieveAllVehicles.rawValue) { (responseObject) -> () in
            switch responseObject {
            case .Success(let vehiclesDict):
                var retrievedVehicles = [BMWVehicle]()
                // Parse our dict
                let parsedDict = JSON(vehiclesDict)
                // Map out values respectively
                for rawVehicle in (parsedDict.object as? [AnyObject])! {
                    let vehicleDict = JSON(rawVehicle)

                    // Differenciate regular and electric vehicle

                    if let _ = vehicleDict[VehicleDataModel.electricCharge.rawValue].number {
                        guard let electricVehicle = Mapper<ElectricVehicle>().map(rawVehicle) else {
                            vehicleData(.Failure(VehicleNetworkingError.CannotParseElectricVehicle))
                            return
                        }
                        retrievedVehicles.append(electricVehicle)
                    } else if let _ = vehicleDict[VehicleDataModel.fuelRange.rawValue].number {
                        guard let regularVehicle = Mapper<RegularVehicle>().map(rawVehicle) else {
                            vehicleData(.Failure(VehicleNetworkingError.CannotParseRegularVehicle))
                            return
                        }
                        retrievedVehicles.append(regularVehicle)
                    } else {
                        guard let mappedVehicle = Mapper<BMWVehicle>().map(rawVehicle) else {
                            vehicleData(.Failure(VehicleNetworkingError.CannotParseVehicle))
                            return
                        }
                        retrievedVehicles.append(mappedVehicle)
                    }
                }
                // Check if our list of vehicles is empty or not

                if retrievedVehicles.isEmpty == true {
                    vehicleData(.Failure(VehicleNetworkingError.NoVehiclesFound))
                } else {
                    self.lastChecked = CACurrentMediaTime()
                    vehicleData(.Success(retrievedVehicles))
                }
            case .Failure(let NetworkingError.ResponseError(responseError)):
                // Pass along the error received
                vehicleData(.Failure(VehicleNetworkingError.UnknownNetworkError(responseError)))
            }
        }
    }

    func retrieveFirstVehicle(shouldReloadManually manualReload: Bool, vehicleData: Result<BMWVehicle, VehicleNetworkingError> -> () )
    {
        if manualReload == false {
            if shouldReloadData() == false { return }
        }
        self.networkingManager.getDataFromEndPoint(VehicleEndpoint.RetrieveFirstVehicle.rawValue) { (responseObject) -> () in
            switch responseObject {
            case .Success(let rawVehicle):
                let vehicleDict = JSON(rawVehicle)

                if let _ = vehicleDict[VehicleDataModel.electricCharge.rawValue].number {
                    guard let electricVehicle = Mapper<ElectricVehicle>().map(rawVehicle) else {
                        vehicleData(.Failure(VehicleNetworkingError.CannotParseElectricVehicle))
                        return
                    }
                    // We reset the last checked time whenever we successfully retrieve the vehicle data
                    VehicleNetworkingFacade.vehicleNetworkingSharedInstance.lastChecked = CACurrentMediaTime()
                    vehicleData(.Success(electricVehicle))
                } else if let _ = vehicleDict[VehicleDataModel.fuelRange.rawValue].number {
                    guard let regularVehicle = Mapper<RegularVehicle>().map(rawVehicle) else {
                        vehicleData(.Failure(VehicleNetworkingError.CannotParseRegularVehicle))
                        return
                    }
                    VehicleNetworkingFacade.vehicleNetworkingSharedInstance.lastChecked = CACurrentMediaTime()
                    vehicleData(.Success(regularVehicle))
                } else {
                    guard let mappedVehicle = Mapper<BMWVehicle>().map(rawVehicle) else {
                        vehicleData(.Failure(VehicleNetworkingError.CannotParseVehicle))
                        return
                    }
                    VehicleNetworkingFacade.vehicleNetworkingSharedInstance.lastChecked = CACurrentMediaTime()
                    vehicleData(.Success(mappedVehicle))
                }
            case .Failure(let NetworkingError.ResponseError(responseError)):
                vehicleData(.Failure(VehicleNetworkingError.UnknownNetworkError(responseError)))
            }
        }
    }
}

/// JSON Facade
class VehicleFacade: HTZNetworkingFacade {

    static let vehicleSharedInstance = VehicleFacade()

    override init() {
        super.init()
        self.networkingManager.baseURL  = VehicleEndpoint.cloudTestURL.rawValue
    }

    func retrieveVehicles(vehicleData: Result<[BMWVehicle], NetworkingError> -> () )
    {
        networkingManager.getDataFromEndPoint(nil) { (JSONResponse) -> () in
            switch JSONResponse {
            case .Success(let vehicleDict):
                var retrievedVehicles: [BMWVehicle]?
                // Parse the JSON object received
                let parsedObject = JSON(vehicleDict)

                let rawObjectDict = parsedObject.object as? NSDictionary
                let JSONDict = JSON(rawObjectDict!)
                // Get the outer array
                let vals = JSONDict["Vehicles"].object as? NSArray
                let innerArray = JSON(vals!)
                // Iterate through the list of vehicles and parse them
                for rawVehicle in (innerArray.object as? [AnyObject])! {
                    let vDict = JSON(rawVehicle)

                    guard let name = vDict[VehicleDataModel.name.rawValue].string else { return }
                    guard let yearMade = vDict[VehicleDataModel.manufacturedYear.rawValue].number else { return }
                    guard let VIN = vDict[VehicleDataModel.VIN.rawValue].string else { return }
                    guard let odometerReading = vDict[VehicleDataModel.odometerReading.rawValue].number else { return }
                    guard let nextServiceMileage = vDict[VehicleDataModel.nextServiceMileage.rawValue].number else { return }
                    guard let lastUpdated = vDict[VehicleDataModel.lastModified.rawValue].string else { return }
                    // We can safely create a basic vehicle
                    if retrievedVehicles == nil {
                        retrievedVehicles = [BMWVehicle]()
                    }

                    // Differenciate regular and electric vehicle or create a basic vehicle if we cannot

                    if let electricCharge = vDict[VehicleDataModel.electricCharge.rawValue].number {
                        guard let electricConsumption = vDict[VehicleDataModel.eletricConsumption.rawValue].number else { return }
                        guard let electricRange = vDict[VehicleDataModel.electricRange.rawValue].number else { return }

                        let electricCar = ElectricVehicle(name: name, odometer: odometerReading.integerValue, nextServiceMileage: nextServiceMileage.integerValue, vehicleVIN: VIN, vehicleManufacturedYear: yearMade.integerValue, electricRange: electricRange.integerValue, totalRange: electricRange.integerValue, electricConsumption: electricConsumption.integerValue, electricCharge: electricCharge.integerValue, lastUpdated: lastUpdated)
                        retrievedVehicles?.append(electricCar)
                    } else if let fuelRange = vDict[VehicleDataModel.fuelRange.rawValue].number {
                        guard let fuelConsumption = vDict[VehicleDataModel.fuelLevel.rawValue].number else { return }

                        let gasolineCar = RegularVehicle(name: name, odometer: odometerReading.integerValue, nextServiceMileage: nextServiceMileage.integerValue, vehicleVIN: VIN, vehicleManufacturedYear: yearMade.integerValue, fuelRange: fuelRange.integerValue, fuelConsumption: fuelConsumption.integerValue, lastUpdated: lastUpdated)
                        retrievedVehicles?.append(gasolineCar)
                    } else {
                        let vehicle = BMWVehicle(name: name, odometer: odometerReading.integerValue, nextServiceMileage: nextServiceMileage.integerValue, vehicleVIN: VIN, vehicleManufacturedYear: yearMade.integerValue, lastUpdated: lastUpdated)
                        retrievedVehicles?.append(vehicle)
                    }
                }
                guard let vehicles = retrievedVehicles else { return }
                vehicleData(.Success(vehicles))
            case .Failure(let NetworkingError.ResponseError(responseError)):
                vehicleData(.Failure(NetworkingError.ResponseError(responseError)))
            }
        }
    }

    func retrieveFirstVehicleFromList(vehicleData: Result<BMWVehicle, NetworkingError> -> () )
    {
        self.retrieveVehicles { (vehicleResponse) -> () in
            switch vehicleResponse {
            case .Success(let vehicles):
                // We should be expecting at least one vehicle (no need to check again)
                vehicleData(.Success(vehicles.first!))
            case .Failure(let NetworkingError.ResponseError(responseError)):
                vehicleData(.Failure(NetworkingError.ResponseError(responseError)))
            }
        }
    }

    func retrieveVehicle(vehicleVIN: String, vehicleData: Result<BasicVehicleDetails, NetworkingError> -> () )
    {
        let endpoint = networkingManager.baseURL! + VehicleEndpoint.RetrieveAllVehicles.rawValue

        networkingManager.getDataFromEndPoint(endpoint) { (responseData) -> () in
            switch responseData {
            case .Success(let rawVehicle):
                let vDict = JSON(rawVehicle)

                //TODO: Refactor to Map to correct type
                guard let name = vDict[VehicleDataModel.name.rawValue].string else { return }
                guard let fuelReserve = vDict["fuelReserve"].number else { return }
                guard let yearMade = vDict["MY"].number else { return }
                guard let VIN = vDict[VehicleDataModel.VIN.rawValue].string else { return }
                guard let odometerReading = vDict[VehicleDataModel.odometerReading.rawValue].number else { return }
                guard let nextServiceMileage = vDict[VehicleDataModel.nextServiceMileage.rawValue].number else { return }

                let vehicle = BasicVehicleDetails(name: name, manufacturedYear: yearMade, VIN: VIN, odometerReading: odometerReading, fuelLevel: fuelReserve, fuelRange: 503, nextServiceMileage: nextServiceMileage)
                vehicleData(.Success(vehicle))
                
            case .Failure(let NetworkingError.ResponseError(responseError)):
                vehicleData(.Failure(NetworkingError.ResponseError(responseError)))
            }
        }
    }
}
