//
//  BMWFacade.swift
//  TestingSwift
//
//  Created by Kemar White on 12/6/15.
//  Copyright © 2015 toohotz. All rights reserved.
//

import Foundation

class BMWFacade {

    static let serviceCenterLocationManager = DealershipLocationManager()

    static let vehicleLocationManager = VehicleLocationManager.globalInstance

    static let vehicleNetworkingManager = VehicleNetworkingFacade.vehicleNetworkingSharedInstance

    static let userActivitySearchManager = UserActivitySearchManager()

    static let localDatabaseManager = FetchedDataController()

    static let connectionManager = ReachabilityManager(usingClosures: true)
}
